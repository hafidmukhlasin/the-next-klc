/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
      // domains: ['klc2.kemenkeu.go.id'], // Add your external image domains here
      remotePatterns: [
        {
          protocol: 'https',
          hostname: 'klc2.kemenkeu.go.id',
          pathname: '**',
        },
      ],
    },
    
  };
  
  export default nextConfig;
  
