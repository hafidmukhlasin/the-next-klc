# KLC NEXT


## Our Stuff

- [Next.js](https://nextjs.org) - React.js Framework
- [TailwindCSS](https://tailwindcss.com) - CSS Frameworks
- [Shadcn UI](https://ui.shadcn.com) - UI Frameworks
    - [Radix UI](https://www.radix-ui.com/) - Base UI Framework for Shadcn
    - [Tanstack Table](https://tanstack.com/table) - Headless Table
- [Zustand](https://zustand-demo.pmnd.rs) - State Management
- [Axios](https://axios-http.com) - HTTP Client


Development
- [Bun](https://bun.sh) - Javascript Runtime
- [JSON Server](https://github.com/typicode/json-server/tree/v0) - Full Fake REST API

## Getting Started

- Install [Bun](https://bun.sh)
- Check bun installation `bun -v`
- Clone this project `git clone https://gitlab.com/hafidmukhlasin/the-next-klc.git`
- Enter to project dir: `cd the-next-klc`
- Install depedency libraries of this project: `bun install`
- Copy & edit .env.local from `example.env`: `cp example.env .env.local`
- Run project: `bun dev`

```sh
$ next dev
  ▲ Next.js 14.2.3
  - Local:        http://localhost:3000
  - Environments: .env.local

 ✓ Starting...
 ✓ Ready in 2.9s
 ```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Try to login SSO.

## Directory Structure
- mock-api  mock api
- public
- src   all code app
    - app   application
        - (auth)    folder page auth
            - login login page
        - (modules) folder modules app
            - _components   components for modules
            - admin folder for module admin
                - dummy folder crud dummy
                    - form  folder for form
                        - page.tsx  main file form crud dummy
                    - columns.tsx  columns defininition for crud dummy
                    - config.ts  config for crud dummy
                    - data-table.tsx  data table for crud dummy
                    - page.tsx  main file crud dummy
            - director folder for module director
            - manager folder for module manager
            - stakeholder folder for module stakeholder
            - team folder for module team
            - trainer folder for module trainer
            - layout.tsx    layout for modules
        - api   folder api for auth
        - layout.tsx    mainlayout app
        - page.tsx  main page app 
    - components    global components
    - context   state management
    - lib   library
    - styles    CSS code
    - types typescript definition
    - auth.config.ts    auth config
    - auth.ts   auth
    - middleware.ts middleware next.js

## Install & Run Fake REST API
- Open terminal, in root project
- Enter to `mock-api` dir: `cd mock-api`
- Install depedency libraries of this project: `npm install`
- Run project: `node index.js`

```sh
JSON Server is running on port 3001
```

Open [http://localhost:3001/dummies](http://localhost:3001/dummies) with your browser to see the result.

for more, check file [README.md](mock-api/README.md)

## How Create CRUD without Coding?

- Open file `mockup-api/db.json`
- add this data in last file

```json
    "tests": [
      { "id": "06f67723-0792-44e9-97ce-0b33b2f14eca", "name": "Test 1" }
    ]
```

- re run fake server: `node index.js`
- open in browser `http://localhost:3001/tests`, You should see:

```json
[
  {
    "id": "06f67723-0792-44e9-97ce-0b33b2f14eca",
    "name": "Test 1"
  }
]
```

- Copy sample crud dummy in `src/app/(modules)/admin/dummy`
- Place in other dir, for example: `src/app/(modules)/admin/test`
- Open file `config.ts` in `test` folder.

```js
export const title = "Test"
export const baseRoute = "/admin/test"
export const baseEndpoint = "http://localhost:3001/tests"
export const fields = [
    { name: 'name', label: 'Nama', type: 'string', min: 3, max: 25, form: 'input:text', required: true, className: 'w-3/4' },
]
```

- Update it, save. and viola, crud test ready now, open in the browser: [http://localhost/admin/test](http://localhost/admin/test)

![](./sc-table.png)

auto generated form, support: select, input:file, input:number, textarea, radiogroup, switch
![](./sc-form.png)

You can update sidebar menu in `src/app/(modules)/admin/layout.tsx`

```js
    const mainMenus = [
        { href: '/admin', label: 'Dashboard', icon: <Home className="h-4 w-4" /> },
        { label: 'Master Data', icon: <Folder className="h-4 w-4" />, items: [
            { href: '/admin/department', label: 'Department', icon: <File className="h-4 w-4" /> },
            { href: '/admin/satker', label: 'Satker', icon: <File className="h-4 w-4" /> },
            { href: '/admin/dummy', label: 'Dummy', icon: <File className="h-4 w-4" /> },

            { href: '/admin/test', label: 'Test', icon: <File className="h-4 w-4" /> }, // <===

        ]},
        { type: 'header', label: 'Header' },
        { href: '/admin/menu1', label: 'Menu1', icon: <File className="h-4 w-4" /> },
        { href: '/admin/menu2', label: 'Menu2', icon: <File className="h-4 w-4" /> },
        { href: '/admin/menu3', label: 'Menu3', icon: <File className="h-4 w-4" /> },
        { type: 'separator', label: '-' },
    ]
```
