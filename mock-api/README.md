# mock-api

Mock API

## Run Mock API Server

To run:

```bash
bun index.js
```

Running http://localhost:3001

## Generate Dummy Data

```bash
bun faker.js
```

It will create file in `./db-dummies.json`