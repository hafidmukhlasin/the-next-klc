const { faker } = require('@faker-js/faker');
const fs = require("fs");

const generateDummiesData = (number) => {
  const dummies = [];
  while (number >= 0) {
    const sex = faker.person.sexType();
    dummies.push({
        id: faker.string.uuid(), // UUID
        image: faker.image.urlLoremFlickr({ category: 'animals' }),
        title: faker.lorem.sentence({ min: 3, max: 7 }),
        description: faker.lorem.sentence({ min: 10, max: 15 }),
        start: faker.date.anytime().toISOString().split('T')[0],
        end: faker.date.anytime().toISOString().split('T')[0],
        company_name: faker.company.name(), // string
        participant_count: faker.number.int({ max: 100 }),
        avatar: faker.image.avatar(),
        first_name: faker.person.firstName(sex),
        last_name: faker.person.lastName(sex),
        sex: sex,
        phone: faker.phone.number(), // number
        email: faker.internet.email(),
        city: faker.location.city(),
        country: faker.location.country(),
        zip_code: faker.location.zipCode(),

        money: faker.number.float({ min: 1000000, max: 100000000 }),
        
        created: faker.date.anytime(),
        status: faker.helpers.arrayElement(['active', 'inactive', 'archived']), // enum
    });
    number--;
  }
  return dummies;
};
fs.writeFileSync(
  "./db-dummies.json",
  JSON.stringify({ dummies: generateDummiesData(10) })
);
