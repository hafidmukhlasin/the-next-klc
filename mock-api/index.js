const cors = require('cors')
const jsonServer = require('json-server')
const path = require('path');
const server = jsonServer.create();
const router = jsonServer.router(path.join(__dirname, 'db.json'));
const middlewares = jsonServer.defaults();

server.use(middlewares);

server.use((req, res, next) => {
  let limit = 5;
  let page = 1;
  if (req.query) {
    limit = parseInt(req.query._limit);
    page = parseInt(req.query._page);
  }
  router.render = (req, res) => {
    let response = res.locals.data
    const totalCount = res.get('x-total-count');

    if (totalCount !== undefined) {   
      response = {
        content: res.locals.data
      };

      let totalPages = 1
      if (limit) {
        totalPages = Math.ceil(totalCount / limit);
      }

      let number = 1
      if (page && page>0) number = page

      response.total_elements = parseInt(totalCount, 10);
      response.total_pages = parseInt(totalPages, 10);
      response.number = parseInt(number, 10);
      response.size = limit;
    }

    res.jsonp(response);
  };
  next();
});

server.use(cors()); // Enable CORS for all origins
server.use(router);

server.listen(3001, () => {
  console.log('JSON Server is running on port 3001');
});