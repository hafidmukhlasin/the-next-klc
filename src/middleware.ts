import NextAuth from 'next-auth';
import { authConfig } from '@/auth.config';
import { DEFAULT_PROTECTED_ROUTE, DEFAULT_PUBLIC_ROUTE, PUBLIC_ROUTES } from '@/lib/routes';

const { auth } = NextAuth(authConfig);

export default auth((req) => {
  const { nextUrl } = req;
  const isAuthenticated = !!req.auth;
  const isPublicRoute = PUBLIC_ROUTES.includes(nextUrl.pathname);

  if (isPublicRoute) {
    if (isAuthenticated) {
      return Response.redirect(new URL(DEFAULT_PROTECTED_ROUTE, nextUrl));
    } 
  } else {
    if (!isAuthenticated) {
      return Response.redirect(new URL(DEFAULT_PUBLIC_ROUTE, nextUrl));
    }
  }
})

export const config = {
 matcher: ['/((?!api|_next/static|_next/image|favicon.ico).*)'],
};
