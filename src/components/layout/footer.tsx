"use client"

import { useSettingsStore } from "@/contexts/store";
import { proper } from "@/lib/helpers";

interface Props {
    children: React.ReactNode;
}
  
const Footer = ({ children }: Props) => {
    const { theme } = useSettingsStore();

    const themeClass = `bg-custom-${theme}`
    return (
        <footer className={`h-12 gap-1 p-4 md:px-6 text-center text-xs ${themeClass}`}>
            {children}
        </footer>
    )
}

export default Footer
