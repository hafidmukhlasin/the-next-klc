"use client"

import { ChevronDown, Menu, Package2, Power } from "lucide-react";
import Link from "next/link";
import { UrlObject } from 'url';
import { Sheet, SheetContent, SheetTrigger } from "../ui/sheet";
import { Button } from "../ui/button";
import NavLink from "../builder/nav-link";
import { Avatar, AvatarImage } from "../ui/avatar";
import { logout } from "@/lib/actions";
import { User } from "next-auth";
import { Separator } from "../ui/separator";
import { Popover, PopoverContent, PopoverTrigger } from "../ui/popover";
import { Label } from "../ui/label";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "../ui/select";
import { useSession } from "next-auth/react";
import { useSettingsStore } from "@/contexts/store";
import { proper } from "@/lib/helpers";
import Header from "./header";

interface MenuItem {
    href: string;
    label: string;
}

interface Props {
    titleApp?: string,
    logoApp?: React.ReactNode;
    linkApp?: UrlObject;
    mainMenus?: MenuItem[];
    userData?: User,
    children?: React.ReactNode;
}

const Navbar = ({ titleApp, logoApp, linkApp, mainMenus = [], userData = {}, children }: Props) => {
    const { update } = useSession()

    const { theme, setTheme } = useSettingsStore();

    const textColor = `text-custom${proper(theme)}Text`

    return (
        <>
        <nav className="hidden flex-col gap-6 text-lg font-medium md:flex md:flex-row md:items-center md:gap-5 md:text-sm lg:gap-6">
            <Link href={linkApp ?? "/"} className={`flex items-center gap-2 text-lg font-semibold md:text-base ${textColor}`}>
                {logoApp ?? <Package2 className="h-6 w-6" />}
                {titleApp ?? <span className="sr-only">Title App</span>}
            </Link>
            {mainMenus.map((menu, index) => (
                <NavLink
                key={index}
                href={menu.href}
                label={menu.label}
                className={`${textColor} hover:opacity-75 transition-colors`}
                />
            ))}
            {children}
        </nav>
        {(mainMenus && mainMenus.length > 0) 
            ? <>
                <Sheet>
                    <SheetTrigger asChild>
                        <Button variant="ghost" size="icon" className="shrink-0 bg-transparent md:hidden">
                        <Menu className="h-5 w-5" />
                        <span className="sr-only">Toggle navigation menu</span>
                        </Button>
                    </SheetTrigger>
                    <SheetContent side="left" className="p-0 w-64">
                        <Header>
                            <Link href={linkApp ?? "/"} className="flex items-center gap-2 text-lg font-semibold">
                                {logoApp}
                            </Link>
                        </Header>
                        <nav className="grid gap-6 text-lg font-medium">
                        <Link href={linkApp ?? "/"} className="flex items-center gap-2 text-lg font-semibold">
                            {logoApp ?? <Package2 className="h-6 w-6" />}
                            {titleApp ?? <span className="sr-only">Title App</span>}
                        </Link>

                        {mainMenus.map((menu, index) => (
                            <NavLink
                            key={index}
                            href={menu.href}
                            label={menu.label}
                            className="text-muted-foreground hover:text-foreground"
                            />
                        ))}
                        </nav>
                    </SheetContent>
                </Sheet>
            </>
            : <>
               <Link href={linkApp ?? "/"} className={`md:hidden flex items-center gap-2 text-lg font-semibold md:text-base ${textColor}`}>
                    {logoApp ?? <Package2 className="h-6 w-6" />}
                    {titleApp ?? <span className="sr-only">Title App</span>}
                </Link> 
            </>}
        <div className="flex-1 overflow-hidden"></div>
        <Separator orientation="vertical" className="h-7 mx-2 custom-separator" />
        <Popover>
            <PopoverTrigger asChild>
                <button className="bg-transparent flex items-center rounded-full">
                    <Avatar className="h-10 w-10">
                        <AvatarImage src={userData?.image_url} alt="Avatar" />
                    </Avatar>
                    <ChevronDown className={textColor} size={15} />
                    <span className="sr-only">Toggle user menu</span>
                </button>
            </PopoverTrigger>
            <PopoverContent className="w-80">
                <div className="grid gap-4">
                <div className="space-y-2">
                    <h4 className="font-medium leading-none">{userData.name}</h4>
                    <p className="text-sm text-muted-foreground">
                    {userData.authority?.name} <br /> 
                    {userData.department?.name} <br /> 
                    {userData.email}
                    </p>
                </div>
                <div className="grid gap-2">
                    <div className="grid grid-cols-3 items-center gap-4">
                        <Label htmlFor="width">Login as</Label>
                        <Select value={userData?.authority?.id} onValueChange={async (val) => {
                            await update({ authority_id: val });
                        }}>
                            <SelectTrigger className="w-[180px]">
                                <SelectValue placeholder="Select a role" />
                            </SelectTrigger>
                            <SelectContent>
                                {userData.authorities?.map((authority: any, index) => {
                                    return <SelectItem key={index} value={authority.id}>{authority.name}</SelectItem>
                                })}
                            </SelectContent>
                        </Select>
                    </div>
                    <div className="grid grid-cols-3 items-center gap-4">
                        <Label htmlFor="width">Theme</Label>
                        <Select value={theme} onValueChange={async (val) => {
                            setTheme(val)
                        }}>
                            <SelectTrigger className="w-[180px]">
                                <SelectValue placeholder="Select a theme" />
                            </SelectTrigger>
                            <SelectContent>
                                {['primary', 'success', 'warning', 'danger', 'info', 'secondary', 'alternate', 'light', 'dark']?.map((theme: any, index) => {
                                    const bgcolor = `bg-custom-`+theme
                                    return <SelectItem key={index} value={theme}><span className={`w-3 h-3 mr-2 ${bgcolor} inline-block rounded-full`}></span>{proper(theme)}</SelectItem>
                                })}
                            </SelectContent>
                        </Select>
                    </div>
                </div>
                </div>
                <Separator className="my-2" />
                <div className="flex">
                    <div className="flex-grow"></div>
                    <Button variant="destructive" size={'sm'} className="rounded-full" onClick={async () => {
                        await logout()
                    }}><Power className="mr-2 h-4 w-4" /> Logout</Button>
                </div>
            </PopoverContent>
        </Popover>
        <div className="hidden md:block text-xs ml-3">
            <div className={`flex flex-col ${textColor} max-w-[125px] overflow-hidden overflow-ellipsis`}>
                <div className="font-bold whitespace-nowrap">{userData.name}</div>
                <div className="font-thin whitespace-nowrap">{userData.authority?.name}</div>
            </div>
        </div>
        </>
    )
}

export default Navbar
