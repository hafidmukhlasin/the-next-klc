"use client"

interface Props {
    children: React.ReactNode;
}
  
const Content = ({ children }: Props) => {
    return (
        <main className="flex min-h-[calc(100vh_-_theme(spacing.16)-100)] flex-1 flex-col gap-4 bg-muted/40 p-4 md:gap-8 md:p-10">
            {children}
        </main>
        
    )
}

export default Content
