"use client"

import React, { useEffect, useState } from 'react';
import { useSettingsStore } from '@/contexts/store';
import { proper } from '@/lib/helpers';

interface Props {
    children: React.ReactNode;
}

const Header = ({ children }: Props) => {
    const theme = useSettingsStore((state) => state.theme);
    const [hydrated, setHydrated] = useState(false);
    useEffect(() => {
      setHydrated(true);
    }, []);

  const themeClass = `bg-custom-${theme}`

  return (
    <header className={`sticky top-0 flex h-16 items-center gap-1 px-4 md:px-6 ${themeClass} border-b-2`}>
      {children}
    </header>
  );
};

export default Header;
