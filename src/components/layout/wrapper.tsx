"use client"

interface Props {
    children: React.ReactNode;
}
  
const Wrapper = ({ children }: Props) => {
    return (
        <div className="flex min-h-screen w-full flex-col">
            {children}
        </div>
    )
}

export default Wrapper
