import axios from "axios";
import axiosClient from "@/lib/axiosClient";
import { Field } from "global";
import { z } from "zod";

export const fetchData = async ({ endpoint, token }: any) => {
    try {
      const baseURL = process.env.NEXT_PUBLIC_API_URL;
      const url = endpoint.startsWith('http') ? endpoint : `${baseURL}${endpoint}`;
      const response = await axios.get(url, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      if (response) {
        const { status, statusText, data } = response
        if (status == 200) {
          return {
            status: 'success',
            message: 'Fetching data successfully',
            data
          }
        } else {
          return {
            status: 'error',
            message: `Error code: ${status}, status: ${statusText}`
          }
        }
      }
    } catch (error: any) {
      if (error.response) {
        const { status, statusText } = error.response
        return {
          status: 'error',
          message: `Error code: ${status}, status: ${statusText}, message: ${error.message}`
        }
      }
      console.error('Fatal error', error.message);
      return {
        status: 'error',
        message: `Fatal error`
      }
    }
};

export const deleteData = async ({ endpoint, id, reload, router, target, toast }: any) => {
  const url = new URL(endpoint);
  url.search = ''; // Clear current search params
  url.pathname += `/${id}`;
  const result = await axiosClient.delete(url.toString());
  if (result && result.status == 200) {
    reload();
    toast({
      title: "Information",
      description: "Delete data successfully.",
    })
    router.push(target)
  } else {
    toast({
      variant: "destructive",
      title: "Uh oh! Something went wrong.",
      description: "There was a problem with your delete data.",
    })
  }
}

export const executeAction = async ({ data, type, toast}: any) => {
  const res = await fetch(`http://localhost:3000/api/dev?type=${type}&action=${data.action}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  })  
  const res2 = await res.json()
  if (res2.status == "success") {
    toast({
      title: "Information",
      description: res2.message,
    })
  } else {
    toast({
      variant: "destructive",
      title: "Uh oh! Something went wrong.",
      description: "There was a problem with your delete data.",
    })
  }
}

export const exportCSV = (data: any[]) => {
  if (data.length > 0) {
    const normalizeData = data.map(row => {
      return row.original
    })
    const headers = Object.keys(normalizeData[0]);

    const csvContent = [headers.join(',')].concat(
      normalizeData.map(row =>
        headers.map(key => (typeof row[key] === 'string' ? `"${row[key]}"` : row[key]))
          .join(',')
      )
    ).join('\n');

    const csvBlob = new Blob([csvContent], { type: 'text/csv;charset=utf-8' });
    const csvUrl = URL.createObjectURL(csvBlob);

    const link = document.createElement('a');
    link.href = csvUrl;
    link.download = 'data.csv';
    link.click();

    URL.revokeObjectURL(csvUrl); // Cleanup
  }
}

export const createSchema = (fields: Field[]) => {
  const schemaObject: { [key: string]: any } = {};
  
  fields
    .filter((field: Field) => field.form !== 'none' && !["parent", "child"].includes(field.type)) 
    .forEach((field) => {
      if (field.type === 'string') {
        if (field.required) {
          if (field.min && field.max) {
            schemaObject[field.name] = z.coerce.string().min(field.min, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).max(field.max, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).trim();
          } 
          else if (field.min) {
            schemaObject[field.name] = z.coerce.string().min(field.min, {
                message: `${field.label} must be at least ${field.min} characters.`,
            }).trim();
          } 
          else if (field.max) {
            schemaObject[field.name] = z.coerce.string().min(1, {
              message: `${field.label} is required.`,
            }).max(field.max, {
                message: `${field.label} must be at most ${field.max} characters.`,
            }).trim();
          } 
          else {
            schemaObject[field.name] = z.coerce.string().min(1, {
              message: `${field.label} is required.`,
            }).trim()
          }
        } else {
          if (field.min && field.max) {
            schemaObject[field.name] = z.coerce.string().min(field.min, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).max(field.max, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).trim().optional().or(z.literal(''));
          } 
          else if (field.min) {
            schemaObject[field.name] = z.coerce.string().min(field.min, {
                message: `${field.label} must be at least ${field.min} characters.`,
            }).trim().optional().or(z.literal(''));
          } 
          else if (field.max) {
            schemaObject[field.name] = z.coerce.string().max(field.max, {
                message: `${field.label} must be at most ${field.max} characters.`,
            }).trim().optional().or(z.literal(''));
          } 
          else {
            schemaObject[field.name] = z.coerce.string().trim().optional().or(z.literal(''));
          }
        }
      }
      else if (field.type === 'email') {
        if (field.required) {
          schemaObject[field.name] = z.coerce.string().trim().email({
            message: `${field.label} invalid email address.`,
          }).min(1, {
            message: `${field.label} is required.`,
          });
        } else {
          schemaObject[field.name] = z.coerce.string().trim().email({
            message: `${field.label} invalid email address.`,
          }).optional().or(z.literal(''));
        }
      } 
      else if (field.type === 'date') {
        if (field.required) {
          schemaObject[field.name] = z.coerce.date({
            required_error: "Date is required",
          }).transform(value => {
            return new Date(value);
          });
        } else {
          schemaObject[field.name] = z.coerce.date({
            required_error: "Date is required",
            // invalid_type_error: "Format invalid",
          }).optional().or(z.literal(''));
        }
      } 
      else if (field.type === 'number') {
        if (field.required) {
            schemaObject[field.name] = z.coerce.number({
                message: `${field.label} must be a number.`,
            })
            .min(1)
        } else {
            schemaObject[field.name] = z.coerce.number({
                message: `${field.label} must be a number.`,
            })
            .min(1)
            .optional().or(z.literal(''));
        }
      }
      else if (field.type === 'enum') {
        let validOptions = field.options || [];
        if (Array.isArray(validOptions) && validOptions.every(option => typeof option === 'object' && option.hasOwnProperty('name'))) {
          validOptions = validOptions.map((option: any) => option.name);
        }
        if (field.required) {
            schemaObject[field.name] = z.coerce.string()
                .min(1, {
                  message: `${field.label} is required.`,
                });
        } else {
            schemaObject[field.name] = z
                .coerce.string()
                .optional()
                .or(z.literal(''));
        }
      }
      else if (field.type === 'boolean') {
        if (field.required) {
            schemaObject[field.name] = z.coerce.boolean({
                message: `${field.label} must be a boolean.`,
            })
        } else {
            schemaObject[field.name] = z.coerce.boolean({
                message: `${field.label} must be a boolean.`,
            })
            .optional().or(z.literal(''));
        }
      }
      else {
        schemaObject[field.name] = z.coerce.string();
      }
  });

  return z.object(schemaObject);
}

export const createDefaultValues = (fields: Field[]) => {
  const defaultValues: { [key: string]: any } = {};
  
  fields
    .filter((field: Field) => field.form !== 'none' && !["parent", "child"].includes(field.type)) 
    .forEach((field) => {
      defaultValues[field.name] = "";
  });

  return defaultValues;
}

export const generateTableNumber = ({ row, table }: any) =>{
  const no = (table.getSortedRowModel()?.flatRows?.findIndex((flatRow: any) => flatRow.id === row.id) || 0);
  const safeNo = Math.abs(no)
  const page = (table.options.state.pagination?.pageIndex || 0)
  const safePage = Math.abs(page) + 1
  const perPage = table.options.state.pagination?.pageSize || 5
  const safePerPage = Math.abs(perPage)
  const initialSequenceNumber = (safePage - 1) * safePerPage;
  const sequenceNumber = initialSequenceNumber + safeNo + 1;
  return sequenceNumber;
}
