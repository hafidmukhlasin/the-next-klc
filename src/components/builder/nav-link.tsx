// src/components/NavLink.tsx

import Link from 'next/link';
import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '../ui/accordion';
import { usePathname } from "next/navigation"
import { MenuItem } from 'global';
import { Separator } from '../ui/separator';

const NavLink: React.FC<MenuItem> = ({ type, href, label, icon, className, items=[], isActive=false, theme}) => {
  const pathname = usePathname()
  const checkActivePath = (path: any) => {
    if (path === pathname) return true
    else {
      const count = (path.match(/\//g) || []).length;
      if (count == 1) return false
      else return pathname.startsWith(path);
    }
  }

  const checkChildrenActivePath = (items: any[]) => {
    const foundItem = items.find(item => checkActivePath(item.href));
    return foundItem ? true : false;
  }

  const defaultClassName = `nav-custom-${theme} hover:bg-gray-200 flex items-center px-3 gap-1 py-1`
  const activeClassName = `nav-custom-${theme}-active`


  return (<>
    {(items && items.length > 0)
      ? <>
        <Accordion type="single" collapsible className="w-full" defaultValue={checkChildrenActivePath(items) ? (href || label) : ''}>
          <AccordionItem value={href || label }>
            <AccordionTrigger className={`${isActive ? activeClassName : ''} justify-start px-4 gap-1 py-2 ${className}`}>{icon} {label} <div className="flex-grow"></div></AccordionTrigger>
            <AccordionContent className='ml-3 px-3 text-xs'>
              {items.map((item, idx) => {
                return <NavLink
                          key={idx}
                          href={item.href}
                          label={item.label}
                          icon={item.icon}
                          items={item.items}
                          className={`${checkActivePath(item.href) ? activeClassName : ''} hover:opacity-75 ${defaultClassName}`}
                          isActive={checkActivePath(item.href)}
                      />
              })}
            </AccordionContent>
          </AccordionItem>
        </Accordion>
      </>
      : <>
        {type == 'header' && <div className={`font-bold text-xs ${defaultClassName} py-2`}>{icon} {label}</div>}
        {type == 'separator' && <Separator className='m-2' />}
        {!['header', 'separator'].includes(type as string) &&
          <Link href={href || ''} className={`${checkActivePath(href) ? activeClassName : ''} hover:opacity-75 ${defaultClassName} ${className}`}>
            {icon} {label}
          </Link>
        }
        
      </>
    }</>);
};

export default NavLink;


// <Link href={href} className={className}>
// {icon} {label}
// </Link>