import { Field } from "global";
import React from "react";
import {
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { Textarea } from "@/components/ui/textarea"
import { Select, SelectContent, SelectGroup, SelectItem, SelectLabel, SelectTrigger, SelectValue } from "@/components/ui/select"
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group"
import { Switch } from "@/components/ui/switch"
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover"
import { Label } from "@/components/ui/label"
import { Button } from "@/components/ui/button"
import { format } from "date-fns"
import { cn } from "@/lib/utils"
import { CalendarIcon } from "lucide-react";
import { Calendar } from "../ui/calendar";

type Props = {
  fields: Field[],
  form: any,
  formStyle: string,
  labelWidth?: number,
  title: string;
}
const FormBuilder = ({ fields, form, formStyle, labelWidth = 100, title }: Props) => {
  return (<>
    {fields
      .filter((field: Field) => field.form !== 'none' && !["parent", "child"].includes(field.type))  
      .map((item: Field, index) => {
        return <React.Fragment key={index}>
          <FormField
            control={form.control}
            name={item.name || 'default_name'}
            render={({ field }) => {
              const formItemClass = ['vertical', 'vertical-compact'].includes(formStyle) ? 'flex flex-col' : 'flex flex-wrap items-center form-horizontal space-y-0 !mt-4';
              const formLabelClass = ['vertical', 'vertical-compact'].includes(formStyle) ? '' : `w-[${labelWidth}px]`;
              const formControlClass  = ['vertical', 'vertical-compact'].includes(formStyle) ? '' : '';
              const elemenClassName = item.className ?? "w-3/4";
              return (
                <FormItem className={formItemClass}>
                  <FormLabel className={formLabelClass}>{item.label} {item.required && '*'}</FormLabel>
                  {(item.form == 'input:text') && <FormControl className={formControlClass}><Input placeholder={item.label} {...field} className={elemenClassName} /></FormControl>}
                  {(item.form == 'input:number') && <FormControl className={formControlClass}><Input type="number" placeholder={item.label} {...field} className={elemenClassName} /></FormControl>}
                  {(item.form == 'input:email') && <FormControl className={formControlClass}><Input type="email" placeholder={item.label}{...field} className={elemenClassName} /></FormControl>}
                  {(item.form == 'input:file') && <FormControl className={formControlClass}><Input type="file" placeholder={item.label} className={elemenClassName} /></FormControl>}
                  {(item.form == 'textarea') && <FormControl className={formControlClass}><Textarea placeholder={item.label}{...field} className={elemenClassName}></Textarea></FormControl>}
                  {(item.form == 'select') && 
                    <Select onValueChange={field.onChange} defaultValue={field.value}>
                      <FormControl className={formControlClass}>
                        <SelectTrigger className="w-[180px]">
                          <SelectValue placeholder={'Select '+item.label} />
                        </SelectTrigger>
                      </FormControl>
                      <SelectContent className={elemenClassName}>
                        {item?.options && item.options.map((option: any) => {
                          return <SelectItem key={option} value={option}>{option}</SelectItem>
                        })}
                      </SelectContent>  
                    </Select>}
                  {(item.form == 'radio-group') && <>
                    <FormControl className={formControlClass}>
                      <RadioGroup
                        onValueChange={field.onChange}
                        defaultValue={field.value}
                        className={item.orientation == 'vertical' ? "flex flex-col space-y-1" : "flex flex-row space-x-1"}
                      >
                        {item?.options && item.options.map((option: any) => {
                          const className = item.orientation == 'vertical' ? 'flex items-center space-x-3 space-y-0' : 'flex items-center space-x-1 space-y-0'
                          return <FormItem  key={option.name} className={className}>
                          <FormControl>
                            <RadioGroupItem value={option.name} />
                          </FormControl>
                          <FormLabel className="font-normal">
                            {option.label}
                          </FormLabel>
                        </FormItem>
                        })}
                      </RadioGroup>
                    </FormControl>
                  </>}
                  {(item.form == 'switch') && <>
                    <FormControl className={formControlClass}>
                      <div className="flex items-center space-x-2">
                          <Switch 
                            id={item.name} 
                            defaultChecked={field.value}
                            onCheckedChange={field.onChange}
                          />
                          <Label htmlFor={item.name}>{item.label}</Label>
                      </div>
                    </FormControl>
                  </>}
                  {(item.form == 'datepicker') && <>
                    <Popover>
                      <PopoverTrigger asChild>
                        <FormControl className={formControlClass}>
                          <Button
                            variant={"outline"}
                            className={cn(
                              "w-[240px] pl-3 text-left font-normal",
                              !field.value && "text-muted-foreground"
                            )}
                          >
                            {field.value ? (
                              format(field.value, "PPP")
                            ) : (
                              <span>Pick a date</span>
                            )}
                            <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                          </Button>
                        </FormControl>
                      </PopoverTrigger>
                      <PopoverContent className="w-auto p-0" align="start">
                        <Calendar
                          mode="single"
                          selected={field.value}
                          onSelect={field.onChange}
                          disabled={(date) =>
                            // date > new Date() || 
                            date < new Date("1900-01-01")
                          }
                          initialFocus
                        />
                      </PopoverContent>
                    </Popover>
                    </>}
                  {(!['input:text', 'input:number', 'input:email', 'input:file', 'textarea', 'select', 'radio-group', 'switch', 'datepicker'].includes(item.form as string)) && 
                    <FormControl className={formControlClass}><Input placeholder={item.label}{...field} className={item.className} /></FormControl>}
                  {(formStyle == 'vertical' as string) &&
                  <FormDescription>
                    {item.label} of {title}.
                  </FormDescription>}
                  <FormMessage />
                </FormItem>
              )
            }}
          />
        </React.Fragment>
      })}
  </>);
};

export default FormBuilder;
