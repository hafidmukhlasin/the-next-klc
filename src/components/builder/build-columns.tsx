"use client"

import { Button } from "@/components/ui/button"
import { ColumnDef } from "@tanstack/react-table"
import { ArrowUpDown } from "lucide-react"
import { Checkbox } from "@/components/ui/checkbox"
import Link from "next/link"
import { Field } from "global"
import { generateTableNumber } from "./helpers"
import { formatDate } from "@/lib/helpers"

const selectColumn: ColumnDef<Field> = {
  id: 'select',
  header: ({ table }) => (
    <Checkbox
      checked={
        table.getIsAllPageRowsSelected() ||
        (table.getIsSomePageRowsSelected() && 'indeterminate')
      }
      onCheckedChange={(value: any) => table.toggleAllPageRowsSelected(!!value)}
      aria-label="Select all"
    />
  ),
  cell: ({ row }) => (
    <Checkbox
      checked={row.getIsSelected()}
      onCheckedChange={(value: any) => row.toggleSelected(!!value)}
      aria-label="Select row"
    />
  ),
  enableSorting: false,
  enableHiding: false,
};

const createColumns = (fields: Field[], baseRoute: string, fieldId: string): ColumnDef<Field>[] => {
  return fields
  .filter(field => field.table !== 'none')  
  .map((field: any) => ({
    accessorKey: field.name,
    id: field.name,
    size: field.width || 0,
    header: ({ table, header }) => (<>
      {field.sortable
        ? <>
          <Button
            variant="ghost"
            onClick={() => {
              header.column.getToggleSortingHandler()
              const sorting = table.getState().sorting
              let desc = false
              if (sorting && sorting[0] && sorting[0].id == field.name) {
                desc = !sorting[0].desc
              }
              table.setSorting([{ id: field.name, desc: desc }])
            }}
          >
            {field.label}
            <ArrowUpDown className="ml-2 h-4 w-4" />
          </Button>
        </>
        : <div className="text-center">{field.label}</div>
      }
    </>),
    cell: ({ row }: any) => {
      const values = row.original || {}
      if (field.type == 'parent') {
        if (values[field.name]) {
          const obj = values[field.name] as any
          return <>{obj?.[field.fieldName as string]}</>
        }
      }
      else if (field.type == 'child') {
        if (values[field.name] && Array.isArray(values[field.name])) {
          return <div className="text-center">
            <Link href={baseRoute+'/'+values[fieldId]}>
              <Button size={'sm'} variant={'outline'}>{values[field.name].length || 0}</Button>
            </Link>
          </div>
        }
      } 
      else if (field.type == 'date') {
        return formatDate(values[field.name])
      }
      else {
        return values[field.name]
      }
    },
  }));
};

export const buildColumns = ({ baseRoute, fieldId, fields }: any ) => {
  const columns: ColumnDef<Field>[] = [
    selectColumn,
    { header: "No", id: "no", size: 50, cell: ({ row, table }) => <div className="text-center">{generateTableNumber({ row, table })}</div> },
    ...createColumns(fields as Field[], baseRoute, fieldId),
    { header: () => <div className="text-center">Action</div>, id:"act", size: 100 }
  ];
  return columns
}