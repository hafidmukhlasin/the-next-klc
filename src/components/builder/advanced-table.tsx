"use client"

import React, { useEffect, useState } from "react"
import { DataTable } from "./data-table"
import { buildColumns } from "./build-columns"
import { SortingState } from "@tanstack/react-table"
import axiosClient from "@/lib/axiosClient"
import { Skeleton } from "../ui/skeleton"


export const AdvancedTable= ({ params, searchParams, baseEndpoint, baseRoute, fieldId, fields, tableAction, parentEndpoint }: any) => {
    const [page, setPage] = useState(searchParams?.page || 1);
    const [perPage, setPerPage] = useState(5);
    const [query, setQuery] = useState("");
    const [sorting, setSorting] = useState<SortingState>([]);
    const [data, setData] = useState([]);
    const [reload, setReload] = useState(true);
    const [pagination, setPagination] = useState({
      totalPages: 0, // total page
      totalElements: 0, // total data
    })

    async function fetchData() {
        const url = new URL(baseEndpoint);

        if (url.search) {
            const existingParams = new URLSearchParams(url.search);
            url.search = '';
            existingParams.forEach((value, key) => {
            url.searchParams.append(key, value);
            });
        }
        url.searchParams.append('_limit', perPage as any);
        url.searchParams.append('_page', page as any);
        if (query) {
            url.searchParams.append('q', query);
        }

        if (sorting && sorting.length >=0 && sorting[0]?.id) {
            url.searchParams.append('_sort', sorting[0]?.id);
            url.searchParams.append('_order', sorting[0]?.desc ? 'desc' : 'asc');
        }
        const result = await axiosClient.get(url.toString());
        if (result && result.data) {
            const { content, total_elements, total_pages } = result.data 
            setData(content);
            setPagination({
                totalPages: total_pages,
                totalElements: total_elements,
            })
            setReload(false)
        }
    }

    useEffect(() => {
        if (reload) fetchData();
      }, [page, perPage, query, reload]);
    
    if (!data) {
    return <div>
        <div className="flex items-center space-x-4">
        <Skeleton className="h-12 w-12 rounded-full" />
        <div className="space-y-2">
            <Skeleton className="h-4 w-[250px]" />
            <Skeleton className="h-4 w-[200px]" />
        </div>
        </div>
    </div>;
    }
    
    return (<>
        <DataTable 
            params={params}
            searchParams={searchParams}
            columns={buildColumns({ baseRoute, fieldId, fields })}
            data={data}
            reloadData={fetchData} 
            totalRows={pagination.totalElements} 
            pagesCount={pagination.totalPages} 
            rowsPerPage={perPage}
            setRowsPerPage={(value): any => {
                setPerPage(value)
                setReload(true)
            }}
            currentPage={page as number} 
            setPage={(value): any => {
                setPage(value)
                setReload(true)
            }}
            query={query}
            setQuery={(value): any => {
                setQuery(value)
                setReload(true)
            }} 
            sorting={sorting as any}
            setSorting={(value: any): any => {
                setSorting(value)
                setReload(true)
            }} 
            baseEndpoint={baseEndpoint}
            baseRoute={baseRoute}
            tableAction={tableAction}
            parentEndpoint={parentEndpoint}
            />
        </>
    )
}
