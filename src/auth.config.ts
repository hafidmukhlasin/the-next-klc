import type { NextAuthConfig } from 'next-auth';
import { fetchData } from './lib/helpers';

interface Authority {
  id: number;
  name: string;
}

export const authConfig = {
  callbacks: {
    async jwt({ token, user, account, profile, trigger, session }) {      
      if (user) {
          token.id = user.id;
      }

      if (account) {
          token.id_token = account.id_token
          token.access_token = account.access_token
          token.refresh_token = account.refresh_token
          token.expires_in = account.expires_in
          let authorities = []
          const response = await fetchData({ 
            endpoint: process.env.MOCK ? 'http://localhost:3001/principal' : '/res/user/principal/me?domain=lms', 
            token: account.access_token
          });
          if (response) {
            const { status, data } = response
            if (status == 'success') {
              token.department = data.departments?.lms
              authorities = data.authorities
              token.authorities = authorities
              token.authority = data.authorities[0]
            }
          }
      }

      if (trigger === "update" && session?.authority_id) {
        const authorities = token.authorities as Authority[]
        console.log(session, authorities)
        const selectedAuthority = authorities.find((a: any) => a.id == session.authority_id)
        if (selectedAuthority) {
          token.authority = selectedAuthority 
        }
      }

      if (profile) {
          token.provider = profile.provider
          token.image_url = profile.image_url
      }
      return token;
    },
    session({ session, token }) {
        if (token) {
            session.user.id = token.id as string;
            session.user.provider = token.provider as string;
            session.user.image_url = token.image_url as string;

            session.user.department = token.department as string;
            session.user.authorities = token.authorities as any[];
            session.user.authority = token.authority as any;
    
            session.token = {
              id_token: token.id_token as string,
              access_token: token.access_token as string,
              refresh_token: token.refresh_token as string,
              expires_in: token.expires_in as number,
            };
        }
        return session;
    },
    authorized({ auth }) {
      const isAuthenticated = !!auth?.user;
      return isAuthenticated;
    },
  },
  providers: [], // Add providers with an empty array for now
} satisfies NextAuthConfig;
