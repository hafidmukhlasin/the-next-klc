import axios from 'axios'
import { getSession } from 'next-auth/react';

const baseURL = process.env.NEXT_PUBLIC_API_URL;

const apiClient = axios.create({
  baseURL,
});

apiClient.interceptors.request.use(
    async (config) => {
      const session = await getSession();
      if (session && session.token) {
        config.headers.Authorization = `Bearer ${session.token.access_token}`;
      }
      return config;
    },
    (error: any) => {
      console.error('Request error:', error);
      return Promise.reject(error);
    },
  );
  
  apiClient.interceptors.response.use(
    (response: any) => response,
    (error: { response: { status: number; }; }) => {
      if (error.response?.status === 401) {
        // Handle unauthorized access (e.g., refresh token or redirect to login)
      }
      return Promise.reject(error);
    },
  );
  

  export default apiClient
