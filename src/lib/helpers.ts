import axios from "axios";

export const fetchData = async ({ endpoint, token }: any) => {
    try {
      const baseURL = process.env.NEXT_PUBLIC_API_URL;
      const url = endpoint.startsWith('http') ? endpoint : `${baseURL}${endpoint}`;
      const response = await axios.get(url, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      if (response) {
        const { status, statusText, data } = response
        if (status == 200) {
          return {
            status: 'success',
            message: 'Fetching data successfully',
            data
          }
        } else {
          return {
            status: 'error',
            message: `Error code: ${status}, status: ${statusText}`
          }
        }
      }
    } catch (error: any) {
      if (error.response) {
        const { status, statusText } = error.response
        return {
          status: 'error',
          message: `Error code: ${status}, status: ${statusText}, message: ${error.message}`
        }
      }
      console.error('Fatal error', error.message);
      return {
        status: 'error',
        message: `Fatal error`
      }
    }
};

export const proper = (text: any) => text.replace(/^[a-z]/, (match: any) => match.toUpperCase());

export const isValidDate = (dateString: string): boolean => {
  const date = new Date(dateString);
  return !isNaN(date.getTime());
};

export const formatDate = (timestamp: string, format: "short" | "normal" | "long" = "normal"): string => {
  if (!isValidDate(timestamp)) {
      return "-";
  }

  const date = new Date(timestamp);
  const dd = String(date.getDate()).padStart(2, '0');
  const mm = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-based
  const year = date.getFullYear();
  const yy = String(year).slice(-2);

  const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
  const month = monthNames[date.getMonth()];
  const mmm = String(month).substring(0, 3);

  if (format === "short") {
    return `${dd}/${mm}/${yy}`;
  }
  else if (format === "long") {
    return `${dd} ${month} ${year}`;
  }
  return `${dd}/${mmm}/${year}`;
};

