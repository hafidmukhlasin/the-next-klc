'use server';

import { signIn, signOut } from '@/auth';
import { DEFAULT_PUBLIC_ROUTE } from './routes';

export async function login() {
  await signIn('keycloak');
}

export async function logout() {
 await signOut({ redirect: true, redirectTo: DEFAULT_PUBLIC_ROUTE});
}