"use client"; // (a)

import * as React from "react";
import { useSettingsStore } from "@/contexts/store";

const Hydration = () => {
  React.useEffect(() => {
    useSettingsStore.persist.rehydrate();
  }, []); // (b)

  return null;
};

export default Hydration;