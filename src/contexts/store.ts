import { create } from "zustand";
import { persist } from "zustand/middleware";

interface SettingsState {
  theme: string;
  setTheme: (newTheme: any) => void;
  sidebar: string;
  setSidebar: (sidebarStatus: any) => void;
}

export const useSettingsStore = create<SettingsState>()(
  persist(
    (set) => ({
      theme: 'primary',
      sidebar: 'open',
      setTheme: (newTheme) => set((state) => ({ theme: newTheme })),
      setSidebar: (sidebarStatus) => set((state) => ({ sidebar: sidebarStatus }))
    }),
    {
      name: "settings",
    }
  )
);