"use client"

import Footer from "@/components/layout/footer";
import Header from "@/components/layout/header";
import MainContent from "@/components/layout/main-content";
import Navbar from "@/components/layout/navbar";
import { Button } from "@/components/ui/button";
import { useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";

export default function Home() {
  const { data } = useSession()

  return (
    <>
      <Header>
        <Navbar 
          logoApp={
            <Image src={'https://klc2.kemenkeu.go.id/shared/assets/images/klc-logo.png'} 
              width={125} 
              height={50} 
              alt="Logo KLC" 
              priority={true}
              style={{ width: 'auto', height: 'auto' }}
            />
          }
          mainMenus={[
            // { href: '/dashboard', label: 'Dashboard' },
            // { href: '/orders', label: 'Orders' },
          ]} 
          userData={data?.user}
        >  
        </Navbar>
      </Header>
      <MainContent>
        <div className="flex flex-1 items-center justify-center rounded-lg border border-dashed shadow-sm">
            <div className="flex flex-col items-center gap-1 text-center">
              <h3 className="text-2xl font-bold tracking-tight">
                Bagaimana kabar Anda hari ini?
              </h3>
              <p className="text-sm text-muted-foreground">
                Apapun itu, semoga baik-baik saja dan tetap semangat dalam bekerja.
              </p>
              <Link href={data?.user?.authority?.route || '/'} className="mt-4">
                <Button>Mulai Bekerja!</Button>
              </Link>
            </div>
        </div> 
      </MainContent> 
      <Footer>Copyright &copy; All Right Reserved</Footer> 
    </>
  );
}





