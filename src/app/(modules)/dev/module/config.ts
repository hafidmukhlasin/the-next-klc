export const title = "Module"
export const baseRoute = "/dev/module"
export const baseEndpoint = "http://localhost:3001/modules?_embed=cruds"
export const fieldId = "id";
export const fieldName = "name";
export const formStyle = "horizontal"; // vertical-compact, vertical, horizontal
export const labelWidth = 100; // if horizontal
export const tableAction: string = "normal"; // dropdown, normal

export const fields = [
    { name: 'name', label: 'Name', type: 'string', min: 3, max: 255, form: 'input:text', required: true, options: [], orientation: "", className: 'w-1/2', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'route', label: 'Route', type: 'string', min: 3, max: 255, form: 'input:text', required: true, options: [], orientation: "", className: 'w-100', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'action', label: 'Action', type: 'enum', min: null, max: null, form: 'select', required: true, options: ['none', 'generate', 'remove'], orientation: "", className: 'w-100', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'cruds', label: 'CRUD', type: 'child', min: null, max: null, form: 'none', required: true, options: [], orientation: "", className: 'w-100', table: "block", width: 35, sortable: false, fieldId: "id", fieldName: "name" },

    // { name: 'image', label: 'Image URL', type: 'string', form: 'input:text', className: 'w-3/4' },
    // { name: 'title', label: 'Title', type: 'string', min: 3, max: 255, form: 'input:text', required: true, className: 'w-3/4' },
    // { name: 'description', label: 'Description', type: 'string', min: 3, form: 'textarea', className: 'w-3/4' },
    // { name: 'start', label: 'Start', type: 'date', form: 'datepicker', required: true, className: 'w-100' },
    // { name: 'end', label: 'End', type: 'date', form: 'datepicker', required: true, className: 'w-100' },
    // { name: 'company_name', label: 'Company Name', type: 'string', min: 3, form: 'input:text', className: 'w-1/3' },
    // { name: 'participant_count', label: 'Participant Count', type: 'number', form: 'input:number', required: false, className: 'w-1/4' },
    // { name: 'avatar', label: 'Avatar', type: 'string', form: 'input:file', required: false },
    // { name: 'first_name', label: 'First Name', type: 'string', min: 3, form: 'input:text', required: true, className: 'w-1/2' },
    // { name: 'last_name', label: 'Last Name', type: 'string', min: 3, form: 'input:text', className: 'w-1/2' },
    // { name: 'gender', label: 'Gender', type: 'boolean', form: 'switch', required: true },
    // { name: 'phone', label: 'Phone', type: 'string', form: 'input:text', className: 'w-100' },
    // { name: 'email', label: 'Email', type: 'email', form: 'input:text', required: false, className: 'w-100' },
    // { name: 'city', label: 'City', type: 'string', form: 'input:text',  className: 'w-100' },
    // { name: 'country', label: 'Country', type: 'string', min: 3, form: 'input:text', className: 'w-100' },
    // { name: 'zip_code', label: 'Zip Code', type: 'string', min: 3, form: 'input:text', className: 'w-100' },
    // { name: 'money', label: 'Money', type: 'number', form: 'input:number', required: false, className: 'w-1/4' },
    // { name: 'created', label: 'Created Date', type: 'date', form: 'datepicker', required: true, className: 'w-100' },
    // { name: 'is_active', label: 'Is Active', type: 'boolean', form: 'switch', required: true },
    // { name: 'display', label: 'Display', type: 'enum', options: ['local', 'public'], form: 'select', required: true, className: 'w-1/4' },
    // { name: 'status', label: 'Status', type: 'enum', options: [{ name: 'active', label: 'Active' }, { name: 'inactive', label: 'Inactive' }, { name: 'archived', label: 'Archived' }], form: 'radio-group', orientation: 'horizontal', required: true, className: 'w-1/4' },
];