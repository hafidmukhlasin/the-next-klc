

"use client"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"
import { format } from "date-fns"
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card"
import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb"
import Link from "next/link"
import { CalendarIcon, MoveLeft, SaveIcon } from "lucide-react"
import { baseEndpoint, baseRoute, fields, formStyle, labelWidth, parentRoute, parentTitle, title } from "../config"
import axiosClient from "@/lib/axiosClient"
import { useToast } from "@/components/ui/use-toast"
import { useRouter, useSearchParams } from "next/navigation"

import React, { useEffect, useState } from "react"
import { Textarea } from "@/components/ui/textarea"
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover"
import { cn } from "@/lib/utils"
import { Calendar } from "@/components/ui/calendar"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/components/ui/select"
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group"
import { Switch } from "@/components/ui/switch"
import { Label } from "@/components/ui/label"
import { Field } from "global"
import { createDefaultValues, createSchema } from "@/components/builder/helpers"
import FormBuilder from "@/components/builder/form-builder"

const formSchema = createSchema(fields as Field[]);

const Page = ({ params, searchParams }: any) => {
  const { toast } = useToast()
  const router = useRouter()
 
  const id = searchParams.id
  const page = searchParams.page || 1

  const [data, setData] = useState(null);

  const realBaseRoute = params.id ? baseRoute.replace(/\[id\]/, params.id as string) : baseRoute;

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: createDefaultValues(fields as Field[]),
  })

  async function fetchData(id: string) {
    const url = new URL(baseEndpoint);
    const existingParams = new URLSearchParams(url.search);
    url.search = ''; // Clear current search params
    url.pathname += `/${id}`;

    existingParams.forEach((value, key) => {
      url.searchParams.append(key, value);
    });

    const result = await axiosClient.get(url.toString());
    if (result && result.data) {
      console.log(result)
      setData(result.data);
      Object.keys(result.data).map((key: any) => {
        const field = fields.find(field => field.name == key)
        if (field) {
          if (field.type == 'date') {
            form.setValue(key, new Date(result.data[key]))
          }
          else if (field.type == 'file') {
            form.setValue(key, result.data[key])
          }
          else {
            form.setValue(key, result.data[key])
          }
        }
      })
    }
  }

  useEffect(() => {
    if (id) fetchData(id as string);
  }, [id]);

  if (!data && id) {
    return <div>Loading...</div>;
  }

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    let result = null
    const url = new URL(baseEndpoint);
    url.search = '';
    if (id) {
      url.pathname += `/${id}`;
      result = await axiosClient.put(url.toString(), (values));
    } else {
      result = await axiosClient.post(url.toString(), (values));
    }
    if (result && result.data) {
      toast({
        title: "Information",
        description: `${id ? 'Update' : 'Create'} data successfully.`,
      })

      router.push(realBaseRoute+'?page='+page)
    } else {
      toast({
        variant: "destructive",
        title: "Uh oh! Something went wrong.",
        description: "There was a problem with your post.",
      })
    }
  }

  return (
    <>
      <Breadcrumb menus={[
        { href: parentRoute, label: parentTitle },
        { href: realBaseRoute, label: title },
        { label: id ? 'Update' : 'Create' },
      ]} />
      <Card>
        <CardHeader className="pt-5 pb-3 border-b bg-gray-100" style={{ borderRadius: "10px 10px 0 0" }}>
          <CardTitle className="flex flex-row space-x-2 items-center">
            <Link href={realBaseRoute}><MoveLeft /></Link> <span className="px-1">{id ? 'Update' : 'Create'} {title}</span>
          </CardTitle>
        </CardHeader>
        <CardContent>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
                <FormBuilder fields={fields as Field[]} form={form} formStyle={formStyle} labelWidth={labelWidth} title={title} />
                
                <Button size={"sm"} type="submit" style={{
                  marginLeft: formStyle == 'horizontal' ? `${labelWidth}px` : ``,
                }}>
                  <SaveIcon className="mr-1 h-4" />Save
                </Button>
            </form>
          </Form>
        </CardContent>
      </Card>
  </>
  )
}

export default Page;
