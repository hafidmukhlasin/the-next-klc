"use client"

import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card";
import { baseRoute, title } from "./config";

const Page = () => {
  return (
    <>
      <Breadcrumb menus={[
        // { label: '...' },
        { href: baseRoute, label: 'Dashboard' },
      ]} />
      <Card>
        <CardHeader>
          <CardTitle>{title}</CardTitle>
          <CardDescription>
            Lorem Ipsum
          </CardDescription>
        </CardHeader>
        <CardContent>
          Lorem Ipsum Sit Dolor Amet.
        </CardContent>
      </Card>
    </>
  )
}

export default Page
