"use client"

import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb"
import { useEffect, useState } from "react"
import axiosClient from "@/lib/axiosClient"
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card"
import { baseRoute, baseEndpoint, title, parentRoute, parentTitle, parentEndpoint, parentFieldName, fieldId, fields } from "./config"
import { Skeleton } from "@/components/ui/skeleton"
import Link from "next/link"
import { MoveLeft } from "lucide-react"
import { AdvancedTable } from "@/components/builder/advanced-table"
import { tableAction } from "../config"

const Page = ({ params, searchParams }: any) => {
  const [parentData, setParentData] = useState();

  async function fetchDataParent() {
    const realParentEndpoint = params.id ? parentEndpoint.replace(/\[parent_id\]/, params.id as string) : parentEndpoint;
    const url = new URL(realParentEndpoint);

    if (url.search) {
        const existingParams = new URLSearchParams(url.search);
        url.search = '';  // Reset search untuk menambah parameter baru
        existingParams.forEach((value, key) => {
        url.searchParams.append(key, value);
        });
    }

    const result = await axiosClient.get(url.toString());
    if (result && result.data) {
        const content = result.data
        setParentData(content);
    }
  }

  useEffect(() => {
      if (parentEndpoint) fetchDataParent()
  }, [])

  if (!parentData) {
    return <div>
      <div className="flex items-center space-x-4">
        <Skeleton className="h-12 w-12 rounded-full" />
        <div className="space-y-2">
          <Skeleton className="h-4 w-[250px]" />
          <Skeleton className="h-4 w-[200px]" />
        </div>
      </div>
    </div>;
  }

  // Replace '[id]' with the custom string
  const realBaseRoute = params.id ? baseRoute.replace(/\[id\]/, params.id as string) : baseRoute;
  return (
    <>
      <Breadcrumb menus={[
        { href: parentRoute, label: parentTitle },
        { href: realBaseRoute, label: title },
      ]} />
      <Card>
          <CardHeader className="pt-5 pb-3 border-b bg-gray-100" style={{ borderRadius: "10px 10px 0 0" }}>
            <CardTitle className="flex flex-row">
              <Link className="inline-block w-[30px]" href={parentRoute}><MoveLeft /></Link> 
              <span className="flex-grow text-center">{parentData?.[parentFieldName]}</span>
              <span className="inline-block w-[30px]">&nbsp;</span>
            </CardTitle>
          </CardHeader>
          <CardHeader className="pt-4 pb-3 border-b bg-gray-50">
            <CardTitle>{title}</CardTitle>
            <CardDescription>
              Manage your {title.toLowerCase()} data.
            </CardDescription>
          </CardHeader>
          <CardContent>
              <AdvancedTable
                params={params}
                searchParams={searchParams}
                baseEndpoint={baseEndpoint} 
                baseRoute={baseRoute}
                fieldId={fieldId} 
                fields={fields}
                tableAction={tableAction}
                parentEndpoint={parentEndpoint}
              />
          </CardContent>
      </Card>
    </>
  )
}

export default Page
