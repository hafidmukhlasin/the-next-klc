import { Field } from "global";

export const title = "Diklat"
export const baseRoute = "/admin/diklat"
export const baseEndpoint = "http://localhost:3001/trainings?_embed=students"
export const fieldId = "id";
export const fieldName = "name";
export const formStyle = "vertical"; // vertical-compact, vertical, horizontal
export const labelWidth = 125; // if horizontal
export const tableAction: string = "normal"; // dropdown, normal

export const fields: Field[] = [
    { name: 'name', label: 'Nama Lengkap', type: 'string', min: 3, max: 255, form: 'input:text', required: true, options: [], orientation: "", className: 'w-1/2', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'start', label: 'Start', type: 'date', min: 3, max: 255, form: 'datepicker', required: true, options: [], orientation: "", className: 'w-100', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'end', label: 'End', type: 'date', min: 3, max: 255, form: 'datepicker', required: true, options: [], orientation: "", className: 'w-100', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'photo', label: 'Photo', type: 'string', min: 3, max: 255, form: 'input:file', required: true, options: [], orientation: "", className: 'w-100', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'status', label: 'Status', type: 'enum', form: 'select', required: true, options: ['draft', 'publish', 'archive'], orientation: "", className: 'w-100', table: "block", width: 100, sortable: true, fieldId: "", fieldName: "" },
    { name: 'students', label: 'Student', type: 'child', fieldId: "id", fieldName: "name" },
];