"use client"

import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb"
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card"
import { AdvancedTable } from "@/components/builder/advanced-table"

import { baseRoute, baseEndpoint, title, fieldId, fields, tableAction } from "./config"

const Page = ({ params, searchParams }: any) => {
  return (
    <>
      <Breadcrumb menus={[
        { href: baseRoute, label: title },
      ]} />
      <Card>
          <CardHeader className="pt-5 pb-3 border-b bg-gray-100" style={{ borderRadius: "10px 10px 0 0" }}>
            <CardTitle>{title}</CardTitle>
            <CardDescription>
              Manage your {title.toLowerCase()} data.
            </CardDescription>
          </CardHeader>
          <CardContent>
              <AdvancedTable 
                params={params}
                searchParams={searchParams}
                baseEndpoint={baseEndpoint} 
                baseRoute={baseRoute}
                fieldId={fieldId} 
                fields={fields}
                tableAction={tableAction} 
              />
          </CardContent>
      </Card>
    </>
  )
}

export default Page
