"use client"

import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card";
import { baseRoute, title } from "./config";

const Page = () => {
  return (
    <>
      <Breadcrumb menus={[
        { href: baseRoute, label: 'Dashboard' },
      ]} />
      <Card>
        <CardHeader>
          <CardTitle>{title}</CardTitle>
          <CardDescription>
            Generate code module & CRUD
          </CardDescription>
        </CardHeader>
        <CardContent>
          Generate code module & CRUD with one click!
        </CardContent>
      </Card>
    </>
  )
}

export default Page
