"use client"

import { useSession } from "next-auth/react";
import Sidebar from "@/app/(modules)/_components/layout/sidebar";
import Header from "@/app/(modules)/_components/layout/header";
import Navbar from "@/app/(modules)/_components/layout/navbar";
import Image from "next/image";
import { File, Home } from "lucide-react";
import MainContent from "@/app/(modules)/_components/layout/main-content";
import { baseRoute, title } from "./config";

interface Props {
    children: React.ReactNode;
}
  
const Layout = ({ children }: Props) => {
    const { data, update } = useSession() 
    const userData= data?.user || {}
    const mainMenus = [
        { href: baseRoute, label: 'Dashboard', icon: <Home /> },
        { href: baseRoute + '/training', label: 'Training', icon: <File /> },
        { href: baseRoute + '/diklat', label: 'Diklat', icon: <File /> },

        { label: 'Header', icon: <File />, items: [
            { href: baseRoute + '/training', label: 'Kalender Pembelajaran', icon: <File /> },
            { href: baseRoute + '/diklat', label: 'Diklat', icon: <File /> },
            { href: '/admin/menu3', label: 'Menu3', icon: <File /> },
        ] },
        
        { type: 'separator', label: '-' },
        { type: 'header', label: 'Header2' },
        { href: '/admin/menu1', label: 'Menu1', icon: <File /> },
        { href: '/admin/menu2', label: 'Menu2', icon: <File /> },
        { href: '/admin/menu3', label: 'Menu3', icon: <File /> },
    ]

    const logoApp = <Image src={'https://klc2.kemenkeu.go.id/shared/assets/images/klc-logo.png'} 
            width={125} 
            height={50} 
            alt="Logo KLC" 
            priority={true}
            style={{ width: 'auto', height: 'auto' }}
        />

    const props = {
        logoApp,
        linkApp: "/",
        titleModule: title, 
        linkModule: baseRoute,
        userData,
        mainMenus,
        updateSession: update
    }
    return (<>
        <Header>
            <Navbar {...props} />
        </Header>

        <div className="flex flex-1 overflow-hidden">
            <Sidebar {...props} />           
            <MainContent>{children}</MainContent>
        </div>
    </>);
};
  
export default Layout;