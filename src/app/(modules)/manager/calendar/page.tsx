"use client"

import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb"

const CalendarPage = () => {
  return (
    <>
      <Breadcrumb menus={[
        // { label: '...' },
        { href: '/director', label: 'Dashboard' },
        { href: '/director/calendar', label: 'Kalender Pembelajaran' },
      ]} />
      Calendar Page
    </>
  )
}

export default CalendarPage
