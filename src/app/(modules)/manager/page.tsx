"use client"

import { useSession } from "next-auth/react";
import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb";

const DirectorPage = () => {
  const { data } = useSession()
  return (
    <>
      <Breadcrumb menus={[
        // { label: '...' },
        { href: '/manager', label: 'Dashboard' },
      ]} />
      Manager Page
    </>
  )
}

export default DirectorPage
