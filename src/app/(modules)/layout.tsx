import Wrapper from "./_components/layout/wrapper";

interface Props {
  children: React.ReactNode;
}

const ModuleLayout = async ({ children }: Props) => {
  return (
    <Wrapper>{children}</Wrapper>
  );
};

export default ModuleLayout;