"use client"

import { Button } from "@/components/ui/button"
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card"
import { useSettingsStore } from "@/contexts/store"
import { useRouter } from "next/navigation"
import { useEffect, useState } from "react"
import { UrlObject, Url } from "url"
import { User } from "next-auth";
import { MenuItem } from "global"
import { Avatar, AvatarImage } from "@/components/ui/avatar"
import { ChevronDown, PanelLeftClose, PanelLeftOpen, Power } from "lucide-react"
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover"
import { Label } from "@/components/ui/label"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/components/ui/select"
import { Separator } from "@/components/ui/separator"
import { proper } from "@/lib/helpers"
import { logout } from "@/lib/actions"
import Link from "next/link"
import NavLink from "@/components/builder/nav-link"

interface Props {
    logoApp?: React.ReactNode;
    linkApp?: string | UrlObject;
    titleModule?: string;
    linkModule?: string | UrlObject | Url;
    userData?: User,
    mainMenus?: MenuItem[];
    children?: React.ReactNode;
    updateSession: any
}

const Sidebar = ({ titleModule = '', linkModule = '', userData = {}, mainMenus = [], children, updateSession }: Props) => {
    const { theme, setTheme, sidebar, setSidebar } = useSettingsStore();
    const router = useRouter();

    const [hydrated, setHydrated] = useState(false);
    useEffect(() => {
      setHydrated(true);
    }, []);

    return (<>
        <aside className={`flex-none ${sidebar == 'open' ? 'w-64' : 'md:hidden'} bg-muted/40 overflow-x-auto hidden md:block border-r`}>
            <div className="flex items-center justify-center p-1 border-b font-bold">
                <span className="flex-grow-0 w-[40px]"></span>
                <span className="flex-grow text-center"><Link href={linkModule}>{titleModule}</Link></span>
                <Button variant={'outline'} className="flex-grow-0 w-[40px] border p-2 rounded" onClick={() => {
                    setSidebar(sidebar == 'open' ? 'close' : 'open')
                }}>
                    {sidebar == 'open' ? <PanelLeftClose /> : <PanelLeftOpen />}
                </Button>
            </div>
            <div className="flex flex-col items-center justify-center gap-1 p-2 mt-2">
                <Popover>
                    <PopoverTrigger asChild>
                        <button className="bg-transparent flex items-center rounded-full">
                            <Avatar className="h-16 w-16">
                                <AvatarImage src={userData?.image_url} alt="Avatar" />
                            </Avatar>
                            <ChevronDown size={15} />
                            <span className="sr-only">Toggle user menu</span>
                        </button>
                    </PopoverTrigger>
                    <PopoverContent className="w-80">
                        <div className="grid gap-2">
                            <div className="grid grid-cols-3 items-center gap-4">
                                <Label htmlFor="width">Login as</Label>
                                <Select value={userData?.authority?.id} onValueChange={async (val) => {
                                    await updateSession({ authority_id: val });
                                    router.push('/')
                                }}>
                                    <SelectTrigger className="w-[180px]">
                                        <SelectValue placeholder="Select a role" />
                                    </SelectTrigger>
                                    <SelectContent>
                                        {/* <SelectGroup> */}
                                        {/* <SelectLabel>Role</SelectLabel> */}
                                        {userData.authorities?.map((authority: any, index) => {
                                            return <SelectItem key={index} value={authority.id}>{authority.name}</SelectItem>
                                        })}
                                        {/* </SelectGroup> */}
                                    </SelectContent>
                                </Select>
                            </div>
                            <div className="grid grid-cols-3 items-center gap-4">
                                <Label htmlFor="width">Theme</Label>
                                <Select value={theme} onValueChange={async (val) => {
                                    setTheme(val)
                                }}>
                                    <SelectTrigger className="w-[180px]">
                                        <SelectValue placeholder="Select a theme" />
                                    </SelectTrigger>
                                    <SelectContent>
                                        {['primary', 'success', 'warning', 'danger', 'info', 'secondary', 'alternate', 'light', 'dark']?.map((theme: any, index) => {
                                            const bgcolor = `bg-custom-`+theme
                                            return <SelectItem key={index} value={theme}><span className={`w-3 h-3 mr-2 ${bgcolor} inline-block rounded-full`}></span>{proper(theme)}</SelectItem>
                                        })}
                                    </SelectContent>
                                </Select>
                            </div>
                        </div>
                        <Separator className="my-2" />
                        <div className="flex justify-center">
                            <Button variant="destructive" size={'sm'} className="rounded-full" onClick={async () => {
                                await logout()
                            }}><Power className="mr-2 h-4 w-4" /> Logout</Button>
                        </div>
                    </PopoverContent>
                </Popover>
                <div className={`flex flex-col justify-center items-center overflow-hidden overflow-ellipsis`}>
                    <div className="text-sm font-bold whitespace-nowrap">{userData.name}</div>
                    <div className="text-xs font-thin whitespace-nowrap">{userData.authority?.name} - {userData.department?.name}</div>
                </div>
            </div>
            <Separator></Separator>
            <div className="flex h-full max-h-screen flex-col gap-2 py-4">
                <div className="flex-1">
                    {children}
                    <nav className="grid items-start text-sm font-medium">
                        {mainMenus.map((menu, index) => (
                            <NavLink
                                key={index}
                                type={menu.type}
                                href={menu.href}
                                label={menu.label}
                                icon={menu.icon}
                                items={menu.items}
                                theme={theme}
                            />
                        ))}
                    </nav>
                </div>
                <div className="mt-auto p-4">
                    <Card x-chunk="dashboard-02-chunk-0">
                    <CardHeader className="p-2 pt-0 md:p-4">
                        <CardTitle>Upgrade to Pro</CardTitle>
                        <CardDescription>
                        Unlock all features and get unlimited access to our support
                        team.
                        </CardDescription>
                    </CardHeader>
                    <CardContent className="p-2 pt-0 md:p-4 md:pt-0">
                        <Button size="sm" className="w-full">
                        Upgrade
                        </Button>
                    </CardContent>
                    </Card>
                </div>
            </div>
        </aside>
    </>)
}

export default Sidebar
