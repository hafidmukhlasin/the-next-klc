"use client"

interface Props {
    children: React.ReactNode;
}
  
const Content = ({ children }: Props) => {
    return (
        // <main className="flex flex-1 flex-col gap-4 p-4 lg:gap-6 lg:p-6 ">
        <main className="flex-1 p-4 bg-slate-100 overflow-x-auto flex flex-col gap-4">
            {children}
        </main>
        
    )
}

export default Content
