"use client"

interface Props {
    children: React.ReactNode;
}

const Wrapper = ({ children }: Props) => {
    return (
        <div className="flex flex-col h-screen">
            {children}
        </div>
    )
}

export default Wrapper
