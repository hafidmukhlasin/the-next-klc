import Link from "next/link"

import {
  Breadcrumb as BreadcrumbRoot,
  BreadcrumbEllipsis,
  BreadcrumbItem,
  BreadcrumbLink,
  BreadcrumbList,
  BreadcrumbPage,
  BreadcrumbSeparator,
} from "@/components/ui/breadcrumb"
import { UrlObject } from "url";
import { Home } from "lucide-react";
import React from "react";

interface MenuItem {
    href?: string | UrlObject;
    label: string;
}

interface Props {
    useHome?: boolean,
    menus?: MenuItem[];
}

export function Breadcrumb({ useHome = true, menus = []}: Props ) {
  return (
    <BreadcrumbRoot className="pl-2">
      <BreadcrumbList>
        {useHome &&
            <BreadcrumbItem>
                <BreadcrumbLink asChild>
                <Link href="/"><Home className="w-4 h-4" /></Link>
                </BreadcrumbLink>
            </BreadcrumbItem>
        }
        {menus.map((menu, index) => {
            if (menu.label == '...') {
                return (<React.Fragment key={index}>
                    <BreadcrumbSeparator />
                    <BreadcrumbItem>
                        <BreadcrumbEllipsis />
                    </BreadcrumbItem>
                </React.Fragment>)
            } else {
                return (<React.Fragment key={index}>
                    <BreadcrumbSeparator />
                    <BreadcrumbItem>
                    <BreadcrumbLink asChild>
                        {menu.href ? <Link href={menu?.href}>{menu.label}</Link> : <Link href={'#'}>{menu.label}</Link>}
                    </BreadcrumbLink>
                    </BreadcrumbItem>
                </React.Fragment>)
            }
        })}
      </BreadcrumbList>
    </BreadcrumbRoot>
  )
}
