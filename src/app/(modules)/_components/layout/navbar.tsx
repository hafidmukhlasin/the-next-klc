"use client"

import { ChevronDown, Menu, Package2, PanelLeftClose, PanelLeftOpen, Power } from "lucide-react";
import Link from "next/link";
import { Url, UrlObject } from 'url';
import { Sheet, SheetContent, SheetTrigger } from "@/components/ui/sheet";
import { Button } from "@/components/ui/button";
import { Avatar, AvatarImage } from "@/components/ui/avatar";
import { logout } from "@/lib/actions";
import { User } from "next-auth";
import { Separator } from "@/components/ui/separator";
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover";
import { Label } from "@/components/ui/label";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "@/components/ui/select";
import { useSettingsStore } from "@/contexts/store";
import { proper } from "@/lib/helpers";
import { useRouter } from "next/navigation";
import { MenuItem } from "global";
import Header from "./header";
import { useState } from "react";
import NavLink from "@/components/builder/nav-link";

interface Props {
    logoApp?: React.ReactNode;
    linkApp?: string | UrlObject;
    titleModule?: string;
    linkModule?: string | UrlObject | Url;
    userData?: User,
    mainMenus?: MenuItem[];
    children?: React.ReactNode;
    updateSession: any;
}

const Navbar = ({ logoApp, linkApp, titleModule = '', linkModule = '', userData = {}, mainMenus = [], children, updateSession }: Props) => {
    const { theme, setTheme, sidebar, setSidebar } = useSettingsStore();
    const [ statusSidebar, setStatusSidebar ] = useState(false)

    const router = useRouter();

    return (
        <>
        <nav className="hidden flex-col gap-1 text-lg font-medium md:flex md:flex-row md:items-center md:gap-1 md:text-sm lg:gap-1">
            {sidebar == 'close' &&
            <Button variant="ghost" size="icon" className={`shrink-0 bg-transparent`} style={{ marginLeft: '-20px' }} onClick={() => {
                setStatusSidebar(!statusSidebar)
            }}>
              <Menu className="h-5 w-5" />
              <span className="sr-only">Toggle navigation menu</span>
            </Button>}
            <Link href={linkApp ?? "/"} className={`flex items-center gap-2 text-lg font-semibold md:text-base`}>
                {logoApp}
            </Link>
            {children}
        </nav>
        <Sheet open={statusSidebar} onOpenChange={(status) => {
            setStatusSidebar(status)
        }}>
          <SheetTrigger asChild>
            <Button variant="ghost" size="icon" className={`shrink-0 md:hidden bg-transparent`}>
              <Menu className="h-5 w-5" />
              <span className="sr-only">Toggle navigation menu</span>
            </Button>
          </SheetTrigger>
          <SheetContent side="left" className="p-0 w-64">
            <Header>
                <Link href={linkApp ?? "/"} className="flex items-center gap-2 text-lg font-semibold">
                    {logoApp}
                </Link>
            </Header>
            <div className="flex items-center justify-center p-1 border-b font-bold">
                <span className="flex-grow-0 w-[40px]"></span>
                <span className="flex-grow text-center"><Link href={linkModule}>{titleModule}</Link></span>
                <Button variant={'outline'} className="flex-grow-0 w-[40px] border p-2 rounded" onClick={() => {
                    setSidebar(sidebar == 'open' ? 'close' : 'open')
                    setStatusSidebar(false)
                }}>
                    {sidebar == 'open' ? <PanelLeftClose /> : <PanelLeftOpen />}
                </Button>
            </div>
            <div className="flex flex-col items-center justify-center gap-1 p-2 mt-2">
                <Popover>
                    <PopoverTrigger asChild>
                        <button className="bg-transparent flex items-center rounded-full">
                            <Avatar className="h-16 w-16">
                                <AvatarImage src={userData?.image_url} alt="Avatar" />
                            </Avatar>
                            <ChevronDown size={15} />
                            <span className="sr-only">Toggle user menu</span>
                        </button>
                    </PopoverTrigger>
                    <PopoverContent className="w-80">
                        <div className="grid gap-2">
                            <div className="grid grid-cols-3 items-center gap-4">
                                <Label htmlFor="width">Login as</Label>
                                <Select value={userData?.authority?.id} onValueChange={async (val) => {
                                    await updateSession({ authority_id: val });
                                    router.push('/')
                                }}>
                                    <SelectTrigger className="w-[180px]">
                                        <SelectValue placeholder="Select a role" />
                                    </SelectTrigger>
                                    <SelectContent>
                                        {/* <SelectGroup> */}
                                        {/* <SelectLabel>Role</SelectLabel> */}
                                        {userData.authorities?.map((authority: any, index) => {
                                            return <SelectItem key={index} value={authority.id}>{authority.name}</SelectItem>
                                        })}
                                        {/* </SelectGroup> */}
                                    </SelectContent>
                                </Select>
                            </div>
                            <div className="grid grid-cols-3 items-center gap-4">
                                <Label htmlFor="width">Theme</Label>
                                <Select value={theme} onValueChange={async (val) => {
                                    setTheme(val)
                                }}>
                                    <SelectTrigger className="w-[180px]">
                                        <SelectValue placeholder="Select a theme" />
                                    </SelectTrigger>
                                    <SelectContent>
                                        {['primary', 'success', 'warning', 'danger', 'info', 'secondary', 'alternate', 'light', 'dark']?.map((theme: any, index) => {
                                            const bgcolor = `bg-custom-`+theme
                                            return <SelectItem key={index} value={theme}><span className={`w-3 h-3 mr-2 ${bgcolor} inline-block rounded-full`}></span>{proper(theme)}</SelectItem>
                                        })}
                                    </SelectContent>
                                </Select>
                            </div>
                        </div>
                        <Separator className="my-2" />
                        <div className="flex justify-center">
                            <Button variant="destructive" size={'sm'} className="rounded-full" onClick={async () => {
                                await logout()
                            }}><Power className="mr-2 h-4 w-4" /> Logout</Button>
                        </div>
                    </PopoverContent>
                </Popover>
                <div className={`flex flex-col justify-center items-center overflow-hidden overflow-ellipsis`}>
                    <div className="text-sm font-bold whitespace-nowrap">{userData.name}</div>
                    <div className="text-xs font-thin whitespace-nowrap">{userData.authority?.name} - {userData.department?.name}</div>
                </div>
            </div>
            <Separator></Separator>
            <nav className="grid items-start p-2 text-sm font-medium lg:px-4">
                {mainMenus.map((menu, index) => (
                    <NavLink
                        key={index}
                        type={menu.type}
                        href={menu.href}
                        label={menu.label}
                        icon={menu.icon}
                        items={menu.items}
                    />
                ))}
            </nav>
          </SheetContent>
        </Sheet>
        <div className="flex-1 overflow-hidden"></div>
        <Separator orientation="vertical" className="h-7 mx-2 bg-customSeparator" />
        <Popover>
            <PopoverTrigger asChild>
                <button className="bg-transparent flex items-center rounded-full">
                    <Avatar className="h-10 w-10">
                        <AvatarImage src={userData?.image_url} alt="Avatar" />
                    </Avatar>
                    <ChevronDown size={15} />
                    <span className="sr-only">Toggle user menu</span>
                </button>
            </PopoverTrigger>
            <PopoverContent className="w-80">
                <div className="grid gap-4">
                <div className="space-y-2">
                    <h4 className="font-medium leading-none">{userData.name}</h4>
                    <p className="text-sm text-muted-foreground">
                    {userData.authority?.name} <br /> 
                    {userData.department?.name} <br /> 
                    {userData.email}
                    </p>
                </div>
                <div className="grid gap-2">
                    <div className="grid grid-cols-3 items-center gap-4">
                        <Label htmlFor="width">Login as</Label>
                        <Select value={userData?.authority?.id} onValueChange={async (val) => {
                            await updateSession({ authority_id: val });
                            router.push('/')
                        }}>
                            <SelectTrigger className="w-[180px]">
                                <SelectValue placeholder="Select a role" />
                            </SelectTrigger>
                            <SelectContent>
                                {/* <SelectGroup> */}
                                {/* <SelectLabel>Role</SelectLabel> */}
                                {userData.authorities?.map((authority: any, index) => {
                                    return <SelectItem key={index} value={authority.id}>{authority.name}</SelectItem>
                                })}
                                {/* </SelectGroup> */}
                            </SelectContent>
                        </Select>
                    </div>
                    <div className="grid grid-cols-3 items-center gap-4">
                        <Label htmlFor="width">Theme</Label>
                        <Select value={theme} onValueChange={async (val) => {
                            setTheme(val)
                        }}>
                            <SelectTrigger className="w-[180px]">
                                <SelectValue placeholder="Select a theme" />
                            </SelectTrigger>
                            <SelectContent>
                                {['primary', 'success', 'warning', 'danger', 'info', 'secondary', 'alternate', 'light', 'dark']?.map((theme: any, index) => {
                                    const bgcolor = `bg-custom-`+theme
                                    return <SelectItem key={index} value={theme}><span className={`w-3 h-3 mr-2 ${bgcolor} inline-block rounded-full`}></span>{proper(theme)}</SelectItem>
                                })}
                            </SelectContent>
                        </Select>
                    </div>
                </div>
                </div>
                <Separator className="my-2" />
                <div className="flex">
                    <div className="flex-grow"></div>
                    <Button variant="destructive" size={'sm'} className="rounded-full" onClick={async () => {
                        await logout()
                    }}><Power className="mr-2 h-4 w-4" /> Logout</Button>
                </div>
            </PopoverContent>
        </Popover>
        <div className="hidden md:block text-xs ml-3">
            <div className={`flex flex-col max-w-[125px] overflow-hidden overflow-ellipsis`}>
                <div className="font-bold whitespace-nowrap">{userData.name}</div>
                <div className="font-thin whitespace-nowrap">{userData.authority?.name}</div>
            </div>
        </div>
        </>
    )
}

export default Navbar
