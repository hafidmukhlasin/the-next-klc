"use client"

import { useSession } from "next-auth/react";
import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb";

const Page = () => {
  const { data } = useSession()
  return (
    <>
      <Breadcrumb menus={[
        // { label: '...' },
        { href: '/director', label: 'Dashboard' },
      ]} />
      Director Page
    </>
  )
}

export default Page
