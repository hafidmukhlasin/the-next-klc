"use client"

import { useSession } from "next-auth/react";
import Sidebar from "@/app/(modules)/_components/layout/sidebar";
import Header from "@/app/(modules)/_components/layout/header";
import Navbar from "@/app/(modules)/_components/layout/navbar";
import Image from "next/image";
import { Calendar, Home } from "lucide-react";
import MainContent from "@/app/(modules)/_components/layout/main-content";
// import Link from "next/link";

interface Props {
    children: React.ReactNode;
}
  
const Layout = ({ children }: Props) => {
    const { data } = useSession() 
    return (<>
        <Sidebar
            logoApp={
                <Image src={'https://klc2.kemenkeu.go.id/shared/assets/images/klc-logo.png'} 
                    width={125} 
                    height={50} 
                    alt="Logo KLC" 
                    priority={true}
                    style={{ width: 'auto', height: 'auto' }}
                />
            }
            mainMenus={[
                { href: '/director', label: 'Dashboard', icon: <Home className="h-4 w-4" /> },
                { href: '/director/calendar', label: 'Kalender Pembelajaran', icon: <Calendar className="h-4 w-4" /> },
            ]}
        />
        <div className="flex flex-col">
            <Header>
                <Navbar titleApp="Director" linkApp={"/director"} userData={data?.user}>  
                </Navbar>
            </Header>
            <MainContent>{children}</MainContent>
        </div>
    </>);
};
  
export default Layout;