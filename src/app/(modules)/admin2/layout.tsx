"use client"

import { useSession } from "next-auth/react";
import Sidebar from "@/app/(modules)/_components/layout/sidebar";
import Header from "@/app/(modules)/_components/layout/header";
import Navbar from "@/app/(modules)/_components/layout/navbar";
import Image from "next/image";
import { File, Folder, Home } from "lucide-react";
import MainContent from "@/app/(modules)/_components/layout/main-content";

interface Props {
    children: React.ReactNode;
}
  
const Layout = ({ children }: Props) => {
    const { data, update } = useSession() 
    const userData= data?.user || {}
    const mainMenus = [
        { href: '/admin', label: 'Dashboard', icon: <Home className="h-4 w-4" /> },
        { label: 'Master Data', icon: <Folder className="h-4 w-4" />, items: [
            { href: '/admin/department', label: 'Department', icon: <File className="h-4 w-4" /> },
            { href: '/admin/satker', label: 'Satker', icon: <File className="h-4 w-4" /> },
            { href: '/admin/dummy', label: 'Dummy', icon: <File className="h-4 w-4" /> },
            { href: '/admin/test', label: 'Test', icon: <File className="h-4 w-4" /> },
        ]},
        { type: 'header', label: 'Header' },
        { href: '/admin/menu1', label: 'Menu1', icon: <File className="h-4 w-4" /> },
        { href: '/admin/menu2', label: 'Menu2', icon: <File className="h-4 w-4" /> },
        { href: '/admin/menu3', label: 'Menu3', icon: <File className="h-4 w-4" /> },
        { type: 'separator', label: '-' },
        { type: 'header', label: 'Header2' },
        { href: '/admin/menu1', label: 'Menu1', icon: <File className="h-4 w-4" /> },
        { href: '/admin/menu2', label: 'Menu2', icon: <File className="h-4 w-4" /> },
        { href: '/admin/menu3', label: 'Menu3', icon: <File className="h-4 w-4" /> },
    ]

    const logoApp = <Image src={'https://klc2.kemenkeu.go.id/shared/assets/images/klc-logo.png'} 
            width={125} 
            height={50} 
            alt="Logo KLC" 
            priority={true}
            style={{ width: 'auto', height: 'auto' }}
        />

    const props = {
        logoApp,
        linkApp: "/",
        titleModule: "Admin", 
        linkModule: "/admin",
        userData,
        mainMenus,
        updateSession: update
    }
    return (<>
        <Header>
            <Navbar {...props} />
        </Header>

        <div className="flex flex-1 overflow-hidden">
            <Sidebar {...props} />           
            <MainContent>{children}</MainContent>
        </div>
    </>);
};
  
export default Layout;