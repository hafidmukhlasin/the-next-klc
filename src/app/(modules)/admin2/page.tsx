"use client"

import { useSession } from "next-auth/react";
import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb";
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card";

const Page = () => {
  return (
    <>
      <Breadcrumb menus={[
        // { label: '...' },
        { href: '/admin', label: 'Dashboard' },
      ]} />
      <Card>
        <CardHeader>
          <CardTitle>Dashboard Page</CardTitle>
          <CardDescription>
            Display dashboard data.
          </CardDescription>
        </CardHeader>
        <CardContent>
          Lorem ipsum
        </CardContent>
      </Card>
    </>
  )
}

export default Page
