"use client"

import { Button } from "@/components/ui/button"
import { ColumnDef } from "@tanstack/react-table"
import { ArrowUpDown } from "lucide-react"
import { Checkbox } from "@/components/ui/checkbox"
import { fields } from "./config"

type Data = {
  [K in typeof fields[number]['name']]: string;
} & { id: string };

const selectColumn: ColumnDef<Data> = {
  id: 'select',
  header: ({ table }) => (
    <Checkbox
      checked={
        table.getIsAllPageRowsSelected() ||
        (table.getIsSomePageRowsSelected() && 'indeterminate')
      }
      onCheckedChange={(value: any) => table.toggleAllPageRowsSelected(!!value)}
      aria-label="Select all"
    />
  ),
  cell: ({ row }) => (
    <Checkbox
      checked={row.getIsSelected()}
      onCheckedChange={(value: any) => row.toggleSelected(!!value)}
      aria-label="Select row"
    />
  ),
  enableSorting: false,
  enableHiding: false,
};

const createColumns = (fields: { name: string; label: string; type: string }[]): ColumnDef<Data>[] => {
  return fields.map((field) => ({
    accessorKey: field.name,
    header: () => (
      <Button
        variant="ghost"
        onClick={() => console.log(`Sorting by ${field.name}`)} // Replace with actual sorting logic
      >
        {field.label}
        <ArrowUpDown className="ml-2 h-4 w-4" />
      </Button>
    ),
  }));
};

export const columns: ColumnDef<Data>[] = [
  selectColumn,
  ...createColumns(fields),
];

