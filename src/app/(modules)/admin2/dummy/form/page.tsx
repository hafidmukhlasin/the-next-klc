

"use client"

import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"
import { format } from "date-fns"
import { Button } from "@/components/ui/button"
import {
  Form,
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form"
import { Input } from "@/components/ui/input"
import { Card, CardContent, CardHeader, CardTitle } from "@/components/ui/card"
import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb"
import Link from "next/link"
import { CalendarIcon, CheckIcon, MoveLeft, SaveIcon } from "lucide-react"
import { baseEndpoint, baseRoute, fields, title } from "../config"
import axiosClient from "@/lib/axiosClient"
import { useToast } from "@/components/ui/use-toast"
import { useRouter, useSearchParams } from "next/navigation"

import React, { useEffect, useState } from "react"
import { Textarea } from "@/components/ui/textarea"
import { Popover, PopoverContent, PopoverTrigger } from "@/components/ui/popover"
import { cn } from "@/lib/utils"
import { Calendar } from "@/components/ui/calendar"
import { Select, SelectContent, SelectGroup, SelectItem, SelectLabel, SelectTrigger, SelectValue } from "@/components/ui/select"
import { CaretSortIcon } from "@radix-ui/react-icons"
import { Command, CommandEmpty, CommandGroup, CommandInput, CommandItem } from "@/components/ui/command"
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group"
import { Switch } from "@/components/ui/switch"
import { Label } from "@/components/ui/label"

interface Field {
  name: string;
  label: string;
  type: string;
  min?: number;
  max?: number;
  form: string;
  required: boolean;
  className?: string;
  options?: any[];
  orientation?: string;
}

const createSchema = (fields: Field[]) => {
  const schemaObject: { [key: string]: any } = {};
  
  fields.forEach((field) => {
      if (field.type === 'string') {
        if (field.required) {
          if (field.min && field.max) {
            schemaObject[field.name] = z.string().min(field.min, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).max(field.max, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).trim();
          } 
          else if (field.min) {
            schemaObject[field.name] = z.string().min(field.min, {
                message: `${field.label} must be at least ${field.min} characters.`,
            }).trim();
          } 
          else if (field.max) {
            schemaObject[field.name] = z.string().min(1, {
              message: `${field.label} is required.`,
            }).max(field.max, {
                message: `${field.label} must be at most ${field.max} characters.`,
            }).trim();
          } 
          else {
            schemaObject[field.name] = z.string().min(1, {
              message: `${field.label} is required.`,
            }).trim()
          }
        } else {
          if (field.min && field.max) {
            schemaObject[field.name] = z.string().min(field.min, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).max(field.max, {
              message: `${field.label} must be between ${field.min} and ${field.max} characters.`,
            }).trim().optional().or(z.literal(''));
          } 
          else if (field.min) {
            schemaObject[field.name] = z.string().min(field.min, {
                message: `${field.label} must be at least ${field.min} characters.`,
            }).trim().optional().or(z.literal(''));
          } 
          else if (field.max) {
            schemaObject[field.name] = z.string().max(field.max, {
                message: `${field.label} must be at most ${field.max} characters.`,
            }).trim().optional().or(z.literal(''));
          } 
          else {
            schemaObject[field.name] = z.string().trim().optional().or(z.literal(''));
          }
        }
      }
      else if (field.type === 'email') {
        if (field.required) {
          schemaObject[field.name] = z.string().trim().email({
            message: `${field.label} invalid email address.`,
          }).min(1, {
            message: `${field.label} is required.`,
          });
        } else {
          schemaObject[field.name] = z.string().trim().email({
            message: `${field.label} invalid email address.`,
          }).optional().or(z.literal(''));
        }
      } 
      else if (field.type === 'date') {
        if (field.required) {
          schemaObject[field.name] = z.date({
            required_error: "Date is required",
            // invalid_type_error: "Format invalid",
          }).transform(value => {
            return new Date(value);
          });
        } else {
          schemaObject[field.name] = z.date({
            required_error: "Date is required",
            // invalid_type_error: "Format invalid",
          }).optional().or(z.literal(''));
        }
      } 
      else if (field.type === 'number') {
        if (field.required) {
            schemaObject[field.name] = z.coerce.number({
                message: `${field.label} must be a number.`,
            })
            .min(1)
        } else {
            schemaObject[field.name] = z.coerce.number({
                message: `${field.label} must be a number.`,
            })
            .min(1)
            .optional().or(z.literal(''));
        }
      }
      else if (field.type === 'enum') {
        let validOptions = field.options || [];
        if (Array.isArray(validOptions) && validOptions.every(option => typeof option === 'object' && option.hasOwnProperty('name'))) {
          validOptions = validOptions.map(option => option.name);
        }
        if (field.required) {
            schemaObject[field.name] = z
                .string()
                .transform(value => {
                  console.log(value)
                    if (validOptions.includes(value)) {
                        return value;
                    } else {
                        throw new Error(`${field.label} is not a valid option. Valid options are: ${validOptions.join(', ')}.`);
                    }
                });
        } else {
            schemaObject[field.name] = z
                .string()
                .optional()
                .transform(value => {
                    if (value === '' || validOptions.includes(value)) {
                        return value;
                    } else {
                        throw new Error(`${field.label} is not a valid option. Valid options are: ${validOptions.join(', ')}.`);
                    }
                })
                .or(z.literal(''));
        }
      }
      else if (field.type === 'boolean') {
        if (field.required) {
            schemaObject[field.name] = z.coerce.boolean({
                message: `${field.label} must be a boolean.`,
            })
        } else {
            schemaObject[field.name] = z.coerce.boolean({
                message: `${field.label} must be a boolean.`,
            })
            .optional().or(z.literal(''));
        }
      }
      else if (field.type === 'text' && field.min) {
        schemaObject[field.name] = z.string().min(field.min, {
            message: `${field.label} must be at least ${field.min} characters.`,
        });
    } 
      else if (field.type === 'text') {
          schemaObject[field.name] = z.string();
      }
  });

  return z.object(schemaObject);
}

const createDefaultValues = (fields: Field[]) => {
  const defaultValues: { [key: string]: any } = {};
  
  fields.forEach((field) => {
      defaultValues[field.name] = "";
  });

  return defaultValues;
}

const formSchema = createSchema(fields as Field[]);

const Page = () => {
  const { toast } = useToast()
  const router = useRouter()
  const searchParams = useSearchParams()
 
  const id = searchParams.get('id')
  const last_page = searchParams.get('last_page')

  const [data, setData] = useState(null);

  // 1. Define your form.
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: createDefaultValues(fields as Field[]),
  })

  async function fetchData(id: string) {
    const result = await axiosClient.get(baseEndpoint+'/'+id);
    if (result && result.data) {
      setData(result.data);
      Object.keys(result.data).map((key: any) => {
        const field = fields.find(field => field.name == key)
        if (field) {
          if (field.type == 'date') {
            form.setValue(key, new Date(result.data[key]))
          }
          else if (field.type == 'file') {
            form.setValue(key, result.data[key])
          }
          else {
            form.setValue(key, result.data[key])
          }
        }
      })
    }
  }

  useEffect(() => {
    if (id) fetchData(id as string);
  }, [id]);

  if (!data && id) {
    return <div>Loading...</div>;
  }

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    let result = null
    if (id) {
      result = await axiosClient.put(baseEndpoint + "/" + id, values);
    } else {
      result = await axiosClient.post(baseEndpoint, values);
    }
    if (result && result.data) {
      toast({
        title: "Information",
        description: `${id ? 'Update' : 'Create'} data successfully.`,
      })
      router.push(baseRoute+'?last_page='+last_page)
    } else {
      toast({
        variant: "destructive",
        title: "Uh oh! Something went wrong.",
        description: "There was a problem with your post.",
      })
    }
  }

  return (
    <>
      <Breadcrumb menus={[
        { label: 'Master Data' },
        { href: baseRoute, label: title },
        { label: id ? 'Update' : 'Create' },
      ]} />
      <Card>
        <CardHeader>
          <CardTitle className="flex flex-row space-x-4">
            <Link href={baseRoute}><MoveLeft /></Link> <span className="p-1">{id ? 'Update' : 'Create'}</span>
          </CardTitle>
        </CardHeader>
        <CardContent>
          <Form {...form}>
            <form onSubmit={form.handleSubmit(onSubmit)} className="space-y-8">
              {fields.map((item: any, index) => {
                return <React.Fragment key={index}>
                  <FormField
                    control={form.control}
                    name={item.name || 'default_name'}
                    render={({ field }) => (
                      <FormItem className="flex flex-col">
                        <FormLabel>{item.label} {item.required && '*'}</FormLabel>
                        {(item.form == 'input:text') && <FormControl><Input placeholder={item.label}{...field} className={item.className} /></FormControl>}
                        {(item.form == 'input:number') && <FormControl><Input type="number" placeholder={item.label}{...field} className={item.className} /></FormControl>}
                        {(item.form == 'input:email') && <FormControl><Input type="email" placeholder={item.label}{...field} className={item.className} /></FormControl>}
                        {(item.form == 'input:file') && <FormControl><Input type="file" placeholder={item.label} className={item.className} /></FormControl>}
                        {(item.form == 'textarea') && <FormControl><Textarea placeholder={item.label}{...field} className={item.className}></Textarea></FormControl>}
                        {(item.form == 'select') && 
                          <Select onValueChange={field.onChange} defaultValue={field.value}>
                            <FormControl>
                              <SelectTrigger className="w-[180px]">
                                <SelectValue placeholder={'Select '+item.label} />
                              </SelectTrigger>
                            </FormControl>
                            <SelectContent className={item.className}>
                              {item.options.map((option: any) => {
                                return <SelectItem key={option} value={option}>{option}</SelectItem>
                              })}
                            </SelectContent>  
                          </Select>}
                        {(item.form == 'radio-group') && <>
                          <FormControl>
                            <RadioGroup
                              onValueChange={field.onChange}
                              defaultValue={field.value}
                              className={item.orientation == 'vertical' ? "flex flex-col space-y-1" : "flex flex-row space-x-1"}
                            >
                              {item.options.map((option: any) => {
                                const className = item.orientation == 'vertical' ? 'flex items-center space-x-3 space-y-0' : 'flex items-center space-x-1 space-y-0'
                                return <FormItem  key={option.name} className={className}>
                                <FormControl>
                                  <RadioGroupItem value={option.name} />
                                </FormControl>
                                <FormLabel className="font-normal">
                                  {option.label}
                                </FormLabel>
                              </FormItem>
                              })}
                            </RadioGroup>
                          </FormControl>
                        </>}
                        {(item.form == 'switch') && <>
                          <FormControl>
                            <div className="flex items-center space-x-2">
                                <Switch 
                                  id={item.name} 
                                  defaultChecked={field.value}
                                  onCheckedChange={field.onChange}
                                />
                                <Label htmlFor={item.name}>{item.label}</Label>
                            </div>
                          </FormControl>
                        </>}
                        {(item.form == 'datepicker') && <>
                          <Popover>
                            <PopoverTrigger asChild>
                              <FormControl>
                                <Button
                                  variant={"outline"}
                                  className={cn(
                                    "w-[240px] pl-3 text-left font-normal",
                                    !field.value && "text-muted-foreground"
                                  )}
                                >
                                  {field.value ? (
                                    format(field.value, "PPP")
                                  ) : (
                                    <span>Pick a date</span>
                                  )}
                                  <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
                                </Button>
                              </FormControl>
                            </PopoverTrigger>
                            <PopoverContent className="w-auto p-0" align="start">
                              <Calendar
                                mode="single"
                                selected={field.value}
                                onSelect={field.onChange}
                                disabled={(date) =>
                                  // date > new Date() || 
                                  date < new Date("1900-01-01")
                                }
                                initialFocus
                              />
                            </PopoverContent>
                          </Popover>
                          </>}
                        {(!['input:text', 'input:number', 'input:email', 'input:file', 'textarea', 'select', 'radio-group', 'switch', 'datepicker'].includes(item.form)) && <FormControl><Input placeholder={item.label}{...field} className={item.className} /></FormControl>}
                        <FormDescription>
                          {item.label} of {title}.
                        </FormDescription>
                        <FormMessage />
                      </FormItem>
                    )}
                  />
                </React.Fragment>
              })}
              <Button type="submit"><SaveIcon className="mr-1 h-5" />Save</Button>
            </form>
          </Form>
        </CardContent>
      </Card>
  </>
  )
}

export default Page;
