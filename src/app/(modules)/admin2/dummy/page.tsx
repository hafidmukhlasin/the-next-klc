"use client"

import { Breadcrumb } from "@/app/(modules)/_components/layout/breadcrumb"
import { columns } from "./columns"
import { DataTable } from "./data-table"
import { useEffect, useState } from "react"
import axiosClient from "@/lib/axiosClient"
import { Card, CardContent, CardDescription, CardHeader, CardTitle } from "@/components/ui/card"
import { baseRoute, baseEndpoint, title } from "./config"
import { Skeleton } from "@/components/ui/skeleton"
import { ScrollArea } from "@/components/ui/scroll-area"
// import { useRouter, useSearchParams } from "next/navigation"

const Page = () => {
  // const searchParams = useSearchParams(); 
  // const last_page = searchParams.get('last_page')
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(5);
  const [query, setQuery] = useState("");
  const [data, setData] = useState([]);
  const [reload, setReload] = useState(true);
  const [pagination, setPagination] = useState({
    totalPages: 0, // total page
    totalElements: 0, // total data
  })

  async function fetchData() {
    const params = new URLSearchParams({
      _limit: perPage,
      _page: page
    } as any);
    
    if (query) params.append('q', query);
    const result = await axiosClient.get(baseEndpoint+"?"+params.toString());
    if (result && result.data) {
      const { content, total_elements, total_pages } = result.data 
      setData(content);
      setPagination({
        totalPages: total_pages,
        totalElements: total_elements,
      })
      setReload(false)
    }
  }

  useEffect(() => {
    if (reload) fetchData();
  }, [page, perPage, query]);

  if (!data) {
    return <div>
      <div className="flex items-center space-x-4">
        <Skeleton className="h-12 w-12 rounded-full" />
        <div className="space-y-2">
          <Skeleton className="h-4 w-[250px]" />
          <Skeleton className="h-4 w-[200px]" />
        </div>
      </div>
    </div>;
  }
  return (
    <>
      <Breadcrumb menus={[
        { label: 'Master Data' },
        { href: baseRoute, label: title },
      ]} />
      <Card>
          <CardHeader>
            <CardTitle>{title}</CardTitle>
            <CardDescription>
              Manage your {title.toLowerCase()} data.
            </CardDescription>
          </CardHeader>
          <CardContent>
              <DataTable 
                columns={columns}
                data={data}
                reloadData={setReload} 
                pagesCount={pagination.totalPages} 
                rowsPerPage={perPage} 
                currentPage={page as number} 
                totalRows={pagination.totalElements} 
                query={query}
                setQuery={(value): any => {
                  setQuery(value)
                  setReload(true)
                }} 
                setPage={(value): any => {
                  setPage(value)
                  setReload(true)
                }} 
                setRowsPerPage={(value): any => {
                  setPerPage(value)
                  setReload(true)
                }} />
          </CardContent>
      </Card>
    </>
  )
}

export default Page
