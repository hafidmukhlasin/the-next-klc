import path from "path";
import fs from "fs";

export async function POST(req: Request) {
    const url = new URL(req.url);
    const type = url.searchParams.get('type');
    const action = url.searchParams.get('action');

    if (type == 'module') {
      const data = await req.json();
      const templateDir = path.join(process.cwd(), '/src/app/(modules)/dev/_templates/module');
      const targetDir = path.join(process.cwd(), '/src/app/(modules)', data.route);

      // console.log("type:", type)
      // console.log("action:", action)
      // console.log("data:", data)
  
      if (action == 'generate') {
        if (!fs.existsSync(targetDir)) {
          fs.mkdirSync(targetDir, { recursive: true });
          console.log('BERHASIL');
        } else {
          console.log(targetDir);
        }
    
        const copyAndCustomizeFiles = () => {
          const filesToCopy = ['config.ts', 'layout.tsx', 'page.tsx'];
      
          filesToCopy.forEach(file => {
            const srcFile = path.join(templateDir, file);
            const destFile = path.join(targetDir, file);
      
            // Jika file adalah config.ts, sesuaikan isi file
            if (file === 'config.ts') {
              const configContent = `
export const title = "${data.title}";
export const baseRoute = "${data.route}";
              `;
      
              fs.writeFileSync(destFile, configContent.trim());
            } else {
              // Salin file lainnya tanpa perubahan
              fs.copyFileSync(srcFile, destFile);
            }
          });
        };
      
        // Jalankan fungsi untuk menyalin dan menyesuaikan file
        copyAndCustomizeFiles();
        return Response.json({
          status: "success", 
          message: `Code generated & files have been copied to ${targetDir}` 
        });
      }
      else if (action == 'remove') {
        if (fs.existsSync(targetDir)) {
          fs.rmSync(targetDir, { recursive: true });
          return Response.json({
            status: "success", 
            message: `Success remove folder ${targetDir}` 
          });
        } else { 
          return Response.json({
            status: "success", 
            message: `No action` 
          });
        }
      } else { 
        return Response.json({
          status: "success", 
          message: `No action` 
        });
      }
    } else {
      return Response.json({ status: "warning",  message: `Select type` });
    }
}