import NextAuth from 'next-auth';

declare module 'next-auth' {
  interface User {
    provider?: string;
    image_url?: string;
    department?: any;
    authorities?: any[];
    authority?: any;
  }

  interface Session {
    token: {
      id_token: string;
      access_token: string;
      refresh_token: string;
      expires_in: number;
    };
  }
}
