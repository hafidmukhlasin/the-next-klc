declare module 'global' {
    interface MenuItem {
        type?: string, // header, separator
        href?: string;
        label: string;
        icon?: React.ReactNode;
        className?: string;
        items?: any[];
        isActive?: boolean,
        theme?: any
    }

    interface Field {
        name: string;
        label: string; // for header tabel | label form field

        type: 'string' | 'url' | 'date' | 'number' | 'enum' | 'email' | 'datetime' | 'boolean' | 'parent' | 'child' ; // data type
        min?: number;
        max?: number;

        form?: 'input:text' | 'input:number' | 'input:email' | 'input:file' | 'textarea' | 'datepicker' | 'switch' | 'select' | 'radio-group' | 'none'; // none for type == parent
        required?: boolean;
        options?: Array<{ name: string; label: string }> | string[];
        orientation?: string; // horizontal | vertical for radio group orientation
        className?: string; // class w-3/4 w-1/2 w-full form form field
        
        table?: string; // none
        width?: number; // column width
        sortable?: boolean; // column sortable

        fieldId?: string; // if type == parent | child
        fieldName?: string; // if type == parent | child

        parentEndpoint?: string; 

        header?: (data: any) => React.ReactNode | string
    }
}